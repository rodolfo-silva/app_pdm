export default {
  white: '#fff',
  lighter: '#f5f5f5',
  light: '#DDD',
  regular: '#999',
  dark: '#666',
  darker: '#333',
  black: '#000',
  blue: '#0c68b1',
  red: '#ed2626',
  green: '#3fb049',

  primary: '#fff55e',

  success: '#9dca83',
  danger: '#e37a7a',

  transparent: 'transparent',
  darkTransparent: 'rgba(0,0,0,0.6)',
  whiteTransparent: 'rgba(255,255,255,0.3)',
};
