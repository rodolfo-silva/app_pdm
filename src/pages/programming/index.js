import React from 'react';
import PropTypes from 'prop-types';

import Header from '~/components/Header';
import Footer from '~/components/Footer';

import background from '~/images/programming/background.png';
import iconFini from '~/images/programming/fini.png';
import iconJundia from '~/images/programming/jundia.png';
import iconShoppingSP from '~/images/programming/shoppingSP.png';

import {
  Container,
  ScrollView,
  Center,
  Content,
  Title,
  Attention,
  AttentionTitle,
  AttentionText,
  Card,
  CardHead,
  CardTitle,
  SectionTitle,
  SectionTitleText,
  CardContent,
  CardContentText,
  Sponsorships,
  SponsorshipsTitle,
  RowIcons,
  Icon,
  Terms,
  TermsText,
} from './styles';

const Programming = ({ navigation }) => (
  <Container source={background} resizeMode="stretch">
    <Header title="PROGRAMAÇÃO DO MÊS" navigation={navigation} historyBack="Main" />
    <ScrollView>
      <Center>
        <Content>
          <Title>Guia de Programação</Title>
          <Title date>Fevereiro de 2019</Title>
          <Attention>
            <AttentionTitle>ATENÇÃO</AttentionTitle>
            <AttentionText>
              As filas das sessões de fotos são liberadas com 20 minutos de antecedência e pode ser
              encerrada antes devido à capacidade de atendimento.
            </AttentionText>
          </Attention>
          <Card>
            <CardHead bg="#ed2626">
              <CardTitle>Casa da Mônica</CardTitle>
            </CardHead>
            <SectionTitle bg="#ffa1a1">
              <SectionTitleText>durante a semana</SectionTitleText>
            </SectionTitle>
            <CardContent>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
            </CardContent>
            <SectionTitle bg="#ffa1a1">
              <SectionTitleText>finais de semana e feriados</SectionTitleText>
            </SectionTitle>
            <CardContent haveBorderRadius>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
            </CardContent>
          </Card>
          <Card>
            <CardHead bg="#3ca732">
              <CardTitle>Vila da Mônica</CardTitle>
            </CardHead>
            <SectionTitle bg="#addea9">
              <SectionTitleText>durante a semana</SectionTitleText>
            </SectionTitle>
            <CardContent>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
            </CardContent>
            <SectionTitle bg="#addea9">
              <SectionTitleText>finais de semana e feriados</SectionTitleText>
            </SectionTitle>
            <CardContent haveBorderRadius>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
            </CardContent>
          </Card>
          <Card>
            <CardHead bg="#007aa7">
              <CardTitle>Ao lado do Palco Principal</CardTitle>
            </CardHead>
            <SectionTitle bg="#4fbde5">
              <SectionTitleText>durante a semana</SectionTitleText>
            </SectionTitle>
            <CardContent>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
            </CardContent>
            <SectionTitle bg="#4fbde5">
              <SectionTitleText>finais de semana e feriados</SectionTitleText>
            </SectionTitle>
            <CardContent haveBorderRadius>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
              <CardContentText>11:15 às 11h40 | Foto com a Mônica e Cebolinha</CardContentText>
            </CardContent>
          </Card>
          <Card>
            <CardHead bg="#d8a509">
              <CardTitle>Palco Principal</CardTitle>
            </CardHead>
            <SectionTitle bg="#ffe082">
              <SectionTitleText>durante a semana</SectionTitleText>
            </SectionTitle>
            <CardContent>
              <CardContentText>
                11:15 às 11h40 | Apresentação Musical com a Turma da Mônica
              </CardContentText>
            </CardContent>
            <SectionTitle bg="#ffe082">
              <SectionTitleText>finais de semana e feriados</SectionTitleText>
            </SectionTitle>
            <CardContent haveBorderRadius>
              <CardContentText>
                11:15 às 11h40 | Apresentação Musical com a Turma da Mônica
              </CardContentText>
            </CardContent>
          </Card>
          <Sponsorships>
            <SponsorshipsTitle>Patrocínios:</SponsorshipsTitle>
            <RowIcons>
              <Icon source={iconFini} />
              <Icon source={iconJundia} />
              <Icon source={iconShoppingSP} />
            </RowIcons>
          </Sponsorships>
          <Terms>
            <TermsText>
              Alvará de Funcionamento Nº 2018/10806-00 Válido até 30/06/2019 - Sujeito à renovação |
              Vistoria Bombeiros Nº 222886 Válido até: 28/01/2019 - Sujeito à renovação
            </TermsText>
          </Terms>
        </Content>
      </Center>
    </ScrollView>
    <Footer navigation={navigation} />
  </Container>
);

Programming.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default Programming;
