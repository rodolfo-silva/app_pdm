import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
  background: #002465;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
`;

export const Center = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: center;
`;

export const Content = styled.View`
  width: 95%;
  margin-top: 20px;
  flex-direction: column;
`;

export const Title = styled.Text`
  font-size: 30px;
  text-align: center;
  font-weight: 700;
  color: ${props => (props.date ? '#ffcc02' : '#ffffff')};
`;

export const Attention = styled.View`
  border-radius: 25px;
  background-color: #ffffff;
  padding: 15px 30px;
  margin: 20px 0 10px;
`;

export const AttentionTitle = styled.Text`
  color: #ed2626;
  font-size: 14px;
  font-weight: 700;
  text-transform: uppercase;
  text-align: center;
  margin-bottom: 10px;
`;

export const AttentionText = styled.Text`
  color: #ed2626;
  font-size: 12px;
  font-weight: 700;
`;

export const Card = styled.View`
  margin: 10px 0;
`;

export const CardHead = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 15px 0;
  background: ${props => props.bg};
  border-top-left-radius: 20px;
  border-top-right-radius: 20px;
`;

export const CardTitle = styled.Text`
  color: #ffffff;
  font-size: 22px;
  font-weight: 700;
`;

export const SectionTitle = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 15px 0;
  background: ${props => props.bg};
`;

export const SectionTitleText = styled.Text`
  color: #363636;
  font-size: 14px;
  font-weight: 700;
  text-transform: uppercase;
`;

export const CardContent = styled.View`
  background: #ffffff;
  padding: 15px;
  border-bottom-left-radius: ${props => (props.haveBorderRadius ? '20px' : '0')};
  border-bottom-right-radius: ${props => (props.haveBorderRadius ? '20px' : '0')};
`;

export const CardContentText = styled.Text`
  color: #363636;
  font-size: 14px;
  font-weight: 700;
  margin-bottom: 10px;
`;

export const Sponsorships = styled.View`
  border-radius: 20px;
  background-color: #ffffff;
  padding: 15px;
  flex-direction: column;
  margin: 20px 0;
`;

export const SponsorshipsTitle = styled.Text`
  color: #363636;
  font-size: 14px;
  font-weight: 700;
  text-align: center;
  margin-bottom: 20px;
`;

export const RowIcons = styled.View`
  flex-direction: row;
  justify-content: space-around;
`;

export const Icon = styled.Image``;

export const Terms = styled.View`
  margin-bottom: 25px;
`;

export const TermsText = styled.Text`
  color: #ffffff;
  font-size: 10px;
  font-weight: 700;
`;
