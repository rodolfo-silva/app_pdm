import React from 'react';
import PropTypes from 'prop-types';

import { Container, MessageView, MessageText } from './styles';

import background from '~/images/background.png';

const Syncronizer = ({ message }) => (
  <Container source={background}>
    <MessageView>
      <MessageText>{message}</MessageText>
    </MessageView>
  </Container>
);

Syncronizer.propTypes = {
  message: PropTypes.string.isRequired,
};

export default Syncronizer;
