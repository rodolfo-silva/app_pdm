import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const MessageView = styled.View`
  border-radius: 5px;
  background: #fff;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 20px 40px;
`;

export const MessageText = styled.Text`
  color: #000;
`;
