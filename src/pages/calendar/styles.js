import styled from 'styled-components/native';
import { Calendar } from 'react-native-calendars';

export const ScrollView = styled.ScrollView`
  flex: 1;
`;

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const Content = styled.View`
  flex: 1;
  padding: 30px 20px 15px;
  flex-direction: column;
  align-items: center;
`;

export const TextIntro = styled.Text`
  color: #fff;
  font-size: 20px;
  font-weight: 700;
  text-align: center;
  margin-bottom: 30px;
  text-transform: uppercase;
`;

export const ContainerHoliday = styled.View`
  background: #fff;
  width: 100%;
  padding-top: 20px;
  padding-left: 20px;
`;

export const HolidayTitle = styled.Text`
  color: #363636;
  font-size: 10px;
  font-weight: 700;
  text-transform: uppercase;
  margin-bottom: 10px;
`;

export const ListHoliday = styled.View`
  width: 100%;
  flex: 1;
  flex-direction: column;
`;

export const ItemHoliday = styled.Text`
  color: #0572ba;
  font-size: 12px;
  font-weight: 700;
  margin-bottom: 5px;
`;

export const CustomCalendar = styled(Calendar)`
  width: 100%;
  border-radius: 6px;
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
`;

export const ContainerLegend = styled.View`
  background: #fff;
  width: 100%;
  padding: 10px 20px 20px;
  border-bottom-left-radius: 6px;
  border-bottom-right-radius: 6px;
  margin-bottom: 20px;
`;

export const CalendarFooterText = styled.Text`
  color: #363636;
  text-transform: uppercase;
  font-size: 10px;
  font-weight: bold;
  text-align: center;
`;

export const LabelLegendText = styled.Text`
  color: #02c1f5;
  font-size: 14px;
  font-weight: bold;
  border-top-width: 1;
  border-color: #02c1f5;
  padding-top: 10px;
  text-transform: uppercase;
`;

export const ListLegends = styled.View`
  margin: 15px 0 10px;
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  flex-wrap: wrap;
`;

export const ItemLegend = styled.View`
  width: 33%;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 12px;
`;

export const Description = styled.Text`
  color: #363636;
  font-size: 9px;
  font-weight: 400;
  max-width: 70px;
  text-align: center;
`;
