import React from 'react';
import PropTypes from 'prop-types';

import { LocaleConfig } from 'react-native-calendars';

import {
  ScrollView,
  Container,
  Content,
  TextIntro,
  ContainerHoliday,
  HolidayTitle,
  ListHoliday,
  ItemHoliday,
  CustomCalendar,
  ContainerLegend,
  CalendarFooterText,
  LabelLegendText,
  ListLegends,
  ItemLegend,
  Description,
} from './styles';

import { Image } from 'react-native';

import Header from '~/components/Header';
import Footer from '~/components/Footer';

import background from '~/images/backgroundBlue.png';
import legend1 from '~/images/calendar/legend1.png';
import legend2 from '~/images/calendar/legend2.png';
import legend3 from '~/images/calendar/legend3.png';
import legend4 from '~/images/calendar/legend4.png';

LocaleConfig.locales.ptbr = {
  monthNames: [
    'Janeiro',
    'Fevereiro',
    'Março',
    'Abril',
    'Maio',
    'Junho',
    'Julho',
    'Agosto',
    'Setembro',
    'Outubro',
    'Novembro',
    'Dezembro',
  ],
  monthNamesShort: [
    'Jan',
    'Fev',
    'Mar',
    'Abr',
    'Mai',
    'Jun',
    'Jul.',
    'Ago',
    'Set.',
    'Out.',
    'Nov.',
    'Dez.',
  ],
  dayNames: ['Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
  dayNamesShort: ['Seg.', 'Ter.', 'Qua.', 'Qui.', 'Sex.', 'Sáb.', 'Dom.'],
};

LocaleConfig.defaultLocale = 'ptbr';

const Calendar = ({ navigation }) => (
  <Container source={background}>
    <Header title="Calendário" navigation={navigation} historyBack="Main" />
    <ScrollView>
      <Content>
        <TextIntro>Clique na data e confira os horários de funcionamento.</TextIntro>
        <CustomCalendar
          theme={{ arrowColor: '#ffcc02' }}
          markingType="custom"
          markedDates={{
            '2019-06-13': {
              customStyles: {
                container: {
                  backgroundColor: '#074698',
                  borderRadius: 4,
                },
                text: {
                  color: '#FFF',
                  fontWeight: 'bold',
                },
              },
            },
            '2019-06-28': {
              customStyles: {
                container: {
                  backgroundColor: '#A2CB1C',
                  borderRadius: 4,
                },
                text: {
                  color: 'black',
                  fontWeight: 'bold',
                },
              },
            },
            '2019-06-29': {
              customStyles: {
                container: {
                  backgroundColor: '#FFE73B',
                  elevation: 2,
                  borderRadius: 4,
                },
                text: {
                  color: 'black',
                },
              },
            },
          }}
        />
        <ContainerHoliday>
          <HolidayTitle>Feriados/ datas comemorativas</HolidayTitle>
          <ListHoliday>
            <ItemHoliday>01. Dia do trabalhador</ItemHoliday>
            <ItemHoliday>02. Dia das mães</ItemHoliday>
          </ListHoliday>
        </ContainerHoliday>
        <ContainerLegend>
          <LabelLegendText>Legendas</LabelLegendText>
          <ListLegends>
            <ItemLegend>
              <Image source={legend1} />
              <Description>Parque aberto das 11h às 19h</Description>
            </ItemLegend>
            <ItemLegend>
              <Image source={legend2} />
              <Description>Parque aberto das 11h às 19h</Description>
            </ItemLegend>
            <ItemLegend>
              <Image source={legend3} />
              <Description>Parque aberto das 11h às 19h</Description>
            </ItemLegend>
            <ItemLegend>
              <Image source={legend4} />
              <Description>Parque aberto das 11h às 19h</Description>
            </ItemLegend>
          </ListLegends>
          <CalendarFooterText>Calendário sujeito à alteração sem aviso prévio</CalendarFooterText>
        </ContainerLegend>
      </Content>
    </ScrollView>
    <Footer navigation={navigation} />
  </Container>
);

Calendar.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default Calendar;
