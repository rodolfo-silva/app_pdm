import styled from 'styled-components/native';
import { colors } from '~/styles';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
`;

export const ShortHeader = styled.ImageBackground`
  width: 100%;
  height: 99px;
  margin-bottom: 20px;
`;

export const ButtonBack = styled.TouchableOpacity`
  margin-left: 25px;
  margin-top: 35px;
`;

export const CenterContent = styled.View`
  flex: 1;
  align-items: center;
`;

export const Form = styled.View`
  width: 90%;
  justify-content: center;
  border-radius: 6px;
`;

export const HeaderForm = styled.View`
  justify-content: center;
  background: #0572ba;
  width: 100%;
  height: 55px;
`;

export const TextHeaderForm = styled.Text`
  font-size: 18px;
  color: #fff;
  text-align: center;
`;

export const ContentForm = styled.View`
  justify-content: center;
  background: ${colors.green};
  width: 100%;
  padding: 20px;
`;

export const TextInput = styled.TextInput.attrs({
  placeholderTextColor: '#a4a4a4',
  fontWeight: 'bold',
})`
  width: 100%;
  height: 48px;
  border-radius: 6px;
  border: 1px solid #2173a1;
  background-color: #ffffff;
  margin-bottom: 20px;
  padding: 0 20px;
`;

export const FormBottom = styled.View`
  margin: 30px 0;
  width: 90%;
`;

export const ContentFormKids = styled.View`
  justify-content: center;
  background: transparent;
  width: 100%;
  padding: 20px;
`;

export const HeaderFormBottom = styled.View`
  justify-content: center;
`;

export const TextHeaderFormBottom = styled.Text`
  color: #363636;
  text-transform: uppercase;
  text-align: center;
  font-weight: bold;
`;

export const TextLabel = styled.Text`
  color: #363636;
  font-weight: bold;
`;

export const ContainerAddKid = styled.View`
  margin-bottom: 40px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const ButtonAdd = styled.ImageBackground`
  width: 30px;
  height: 30px;
`;

export const TextAdd = styled.Text`
  font-size: 14px;
  color: #363636;
  font-weight: bold;
  margin-left: 15px;
`;

export const ContainerTerms = styled.View`
  margin-bottom: 40px;
  flex-direction: row;
`;

export const CheckboxBackground = styled.ImageBackground`
  width: 30px;
  height: 30px;
  margin-left: 20px;
`;

export const TermsText = styled.Text`
  color: #363636;
  font-size: 10px;
  font-weight: 400;
  text-align: center;
  margin-left: 10px;
  width: 70%;
`;

export const ButtonSave = styled.TouchableOpacity`
  border-radius: 6px;
  background-color: #ed2626;
  height: 48px;
  align-items: center;
  justify-content: center;
`;

export const ButtonSaveText = styled.Text`
  color: #ffffff;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
`;
