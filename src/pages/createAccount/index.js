/* eslint-disable max-len */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Images
import shortHeader from '~/images/shortHeader.png';
import background from '~/images/background.png';
import buttonBack from '~/images/icons/buttonBack.png';
import checkBox from '~/images/icons/checkbox.png';
import buttonAdd from '~/images/icons/buttonAdd.png';

import { StyleSheet, Image } from 'react-native';

import {
  Container,
  ScrollView,
  ShortHeader,
  ButtonBack,
  CenterContent,
  Form,
  HeaderForm,
  TextHeaderForm,
  ContentForm,
  TextInput,
  FormBottom,
  HeaderFormBottom,
  TextHeaderFormBottom,
  ContentFormKids,
  TextLabel,
  ContainerAddKid,
  ContainerTerms,
  CheckboxBackground,
  TermsText,
  ButtonSave,
  ButtonSaveText,
  ButtonAdd,
  TextAdd,
} from './styles';

const styles = StyleSheet.create({
  headerForm: {
    borderTopLeftRadius: 6,
    borderTopRightRadius: 6,
  },
  contentForm: {
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
  },
});

class CreateAccount extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  goPage = (page) => {
    const { navigation } = this.props;
    navigation.navigate(page);
  };

  render() {
    return (
      <Container source={background}>
        <ScrollView>
          <ShortHeader source={shortHeader}>
            <ButtonBack
              onPress={() => {
                this.goPage('Main');
              }}
            >
              <Image source={buttonBack} />
            </ButtonBack>
          </ShortHeader>
          <CenterContent>
            <Form>
              <HeaderForm style={styles.headerForm}>
                <TextHeaderForm>Criando sua Conta</TextHeaderForm>
              </HeaderForm>
              <ContentForm style={styles.contentForm}>
                <TextInput
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="Nome Completo"
                  underlineColorAndroid="transparent"
                />
                <TextInput
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="E-mail"
                  underlineColorAndroid="transparent"
                />
                <TextInput
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="Telefone"
                  underlineColorAndroid="transparent"
                />
                <TextInput
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="Senha"
                  underlineColorAndroid="transparent"
                  secureTextEntry
                />
                <TextInput
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="Confirmação da senha"
                  underlineColorAndroid="transparent"
                  secureTextEntry
                />
              </ContentForm>
            </Form>
            <FormBottom>
              <HeaderFormBottom>
                <TextHeaderFormBottom>Informações das Crianças</TextHeaderFormBottom>
              </HeaderFormBottom>
              <ContentFormKids>
                <TextLabel>Nome</TextLabel>
                <TextInput
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="Pedro da Silva"
                  underlineColorAndroid="transparent"
                />
                <TextLabel>Data de Nascimento</TextLabel>
                <TextInput
                  autoCapitalize="none"
                  autoCorrect={false}
                  placeholder="16/04/2015"
                  underlineColorAndroid="transparent"
                />
              </ContentFormKids>
              <ContainerAddKid>
                <ButtonAdd source={buttonAdd} />
                <TextAdd>Adicionar mais uma criança</TextAdd>
              </ContainerAddKid>
              <ContainerTerms>
                <CheckboxBackground source={checkBox} />
                <TermsText>Ao criar uma conta, você concorda com a nossa Termos de serviço e política de privacidade</TermsText>
              </ContainerTerms>
              <ButtonSave
                onPress={() => {
                  this.goPage('Home');
                }}
              >
                <ButtonSaveText>finalizar cadastro</ButtonSaveText>
              </ButtonSave>
            </FormBottom>
          </CenterContent>
        </ScrollView>
      </Container>
    );
  }
}

export default CreateAccount;
