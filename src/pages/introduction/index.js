/* eslint-disable react/destructuring-assignment */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-raw-text */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Carousel, { Pagination } from 'react-native-snap-carousel';

// Images
import background from '~/images/backgroundBlue.png';
import buttonClose from '~/images/icons/buttonClose.png';
import slide1 from '~/images/introduction/slide1.png';
import slide2 from '~/images/introduction/slide2.png';

import { View, Dimensions } from 'react-native';

import {
  Container,
  ScrollView,
  CenterContent,
  Form,
  HeaderForm,
  ImageClose,
  TextHeaderForm,
  ContentForm,
  Image,
  InfoLabel,
  ButtonSend,
  ButtonSendText,
} from './styles';

const { width: viewportWidth } = Dimensions.get('window');
const recalculateWidth = viewportWidth - 40;

class Introduction extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  state = {
    activeSlide: 0,
    slides: [
      {
        title: 'Bem Vindo ao aplicativo do Parque da Mônica!',
        thumbnail: slide1,
        info: 'Uma experiência completa para você usar antes, durante e depois do passeio.',
        navigation: this.props.navigation,
      },
      {
        title: 'Muita diversão na tela do seu celular!',
        thumbnail: slide2,
        info:
          'Informações, novidades, jogos, vídeos e muito mais sobre o maior Parque coberto da América Latina!',
        button: 'Entrar no aplicativo',
        navigation: this.props.navigation,
      },
    ],
  };

  get pagination() {
    const { slides, activeSlide } = this.state;
    return (
      <Pagination
        dotsLength={slides.length}
        activeDotIndex={activeSlide}
        // eslint-disable-next-line react-native/no-inline-styles
        dotStyle={{
          width: 15,
          height: 15,
          borderRadius: 10,
          marginHorizontal: 2,
          backgroundColor: '#0572ba',
        }}
        inactiveDotStyle={{
          backgroundColor: '#FFF',
        }}
        inactiveDotOpacity={1}
        inactiveDotScale={1}
      />
    );
  }

  handleSnapToItem(index) {
    this.setState({ activeSlide: index });
  }

  // eslint-disable-next-line class-methods-use-this
  renderItem({ item }) {
    return (
      <View>
        <HeaderForm>
          <ImageClose source={buttonClose} />
          <TextHeaderForm>{item.title}</TextHeaderForm>
        </HeaderForm>
        <ContentForm>
          <Image source={item.thumbnail} />
          <InfoLabel>{item.info}</InfoLabel>
          {item.button ? (
            <ButtonSend
              onPress={() => {
                item.navigation.navigate('Home');
              }}
            >
              <ButtonSendText>{item.button}</ButtonSendText>
            </ButtonSend>
          ) : null}
        </ContentForm>
      </View>
    );
  }

  render() {
    const { slides } = this.state;
    return (
      <Container source={background}>
        <ScrollView>
          <CenterContent>
            <Form>
              <Carousel
                ref={(c) => {
                  this.carousel = c;
                }}
                data={slides}
                renderItem={this.renderItem}
                sliderWidth={recalculateWidth}
                itemWidth={recalculateWidth}
                onSnapToItem={(index) => {
                  this.handleSnapToItem(index);
                }}
              />
            </Form>
            {this.pagination}
          </CenterContent>
        </ScrollView>
      </Container>
    );
  }
}

export default Introduction;
