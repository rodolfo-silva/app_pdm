import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
`;

export const ShortHeader = styled.ImageBackground`
  width: 100%;
  height: 99px;
  margin-bottom: 20px;
`;

export const ButtonBack = styled.TouchableOpacity`
  width: 19px;
  height: 15px;
  margin-left: 25px;
  margin-top: 35px;
`;

export const CenterContent = styled.View`
  flex: 1;
  align-items: center;
`;

export const Form = styled.View`
  justify-content: center;
  border-radius: 6px;
  background-color: #e8f6fb;
  margin-top: 40px;
  height: 480px;
`;

export const HeaderForm = styled.View`
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 55px;
  margin-bottom: 30px;
`;

export const ImageClose = styled.Image`
  left: 15px;
  top: 15px;
  position: absolute;
`;

export const TextHeaderForm = styled.Text`
  font-size: 18px;
  color: #ed2626;
  text-align: center;
  width: 200px;
  margin-top: 70px;
`;

export const ContentForm = styled.View`
  justify-content: center;
  align-items: center;
  width: 100%;
  padding: 20px;
  flex-direction: column;
`;

export const Image = styled.Image`
  margin-bottom: 20px;
`;

export const InfoLabel = styled.Text`
  color: #363636;
  font-size: 14px;
  font-weight: 700;
  margin-bottom: 25px;
  text-align: center;
`;

export const ButtonSend = styled.TouchableOpacity`
  border-radius: 6px;
  background-color: #ed2626;
  padding: 10px 20px;
  align-items: center;
  justify-content: center;
`;

export const ButtonSendText = styled.Text`
  color: #ffffff;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
`;
