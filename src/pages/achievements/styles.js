import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background: #fff;
  align-items: stretch;
  justify-content: center;
  padding: 30px;
`;

export const Text = styled.Text`
  padding: 20px;
  font-weight: bold;
  text-transform: uppercase;
  text-align: center;
`;

export const Button = styled.TouchableOpacity`
  background: #0366d6;
  border-radius: 4px;
  height: 35px;
  align-items: center;
  justify-content: center;
  margin-top: 10px;
`;

export const ButtonText = styled.Text`
  color: #fff;
  font-size: 16px;
  font-weight: bold;
`;
