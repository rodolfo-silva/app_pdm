import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
  flex-direction: column;
  padding: 15px;
`;

export const BoxContent = styled.View`
  background: #fff;
  border-radius: 5px;
  padding: 20px;
  margin-bottom: 40px;
`;

export const Text = styled.Text`
  color: #363636;
  font-size: 12px;
  font-weight: 700;
  line-height: 18px;
  margin-bottom: 10px;
`;

export const Infos = styled.View`
  margin: 20px 0;
`;

export const InfoRow = styled.View`
  margin-bottom: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Icon = styled.Image``;

export const InfosText = styled.Text`
  color: #363636;
  font-size: 12px;
  font-weight: 700;
  line-height: 18px;
  width: 90%;
`;

export const Form = styled.View`
  margin-top: 20px;
`;

export const Row = styled.View`
  margin-bottom: 10px;
  flex-direction: column;
  align-items: ${props => (props.center ? 'center' : 'flex-start')};
`;

export const RowColumn = styled.View`
  margin-bottom: 10px;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;

export const Column = styled.View`
  width: ${props => props.width};
`;

export const Label = styled.Text`
  color: #363636;
  font-size: 12px;
  font-weight: 700;
  margin-bottom: 10px;
`;

export const InputText = styled.TextInput.attrs(props => ({
  placeholderTextColor: '#a4a4a4',
  fontWeight: 'bold',
  textAlignVertical: props.multiline ? 'top' : 'auto',
}))`
  width: 100%;
  height: ${props => (props.multiline ? '225px' : 'auto')};
  margin-top: ${props => (props.multiline ? '10px' : '0')};
  padding: 10px 20px;
  border-radius: 9px;
  border: 1px solid #2f658e;
  background-color: #ffffff;
  font-weight: 700;
`;

export const PickerContainer = styled.View`
  width: 100%;
  border-radius: 9px;
  border: 1px solid #2f658e;
  padding: 5px;
`;

export const Picker = styled.Picker`
  width: 100%;
  height: 40px;
  border-radius: 9px;
  font-weight: 700;
`;

export const RowTerms = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: center;
  margin: 20px 0;
`;

export const CheckboxBackground = styled.ImageBackground`
  width: 30px;
  height: 30px;
`;

export const TermsText = styled.Text`
  color: #363636;
  font-size: 11px;
  font-weight: 700;
  text-align: center;
  width: 200px;
  margin-left: 15px;
`;

export const ButtonSubmit = styled.TouchableOpacity`
  width: 60%;
  background-color: #ed2626;
  border-radius: 5px;
  padding: 12px 0;
  flex-direction: row;
  justify-content: center;
`;

export const ButtonSubmitText = styled.Text`
  color: #ffffff;
  font-size: 16px;
  font-weight: 400;
  text-transform: uppercase;
`;
