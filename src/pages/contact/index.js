import React from 'react';
import PropTypes from 'prop-types';

import Header from '~/components/Header';
import Footer from '~/components/Footer';

import background from '~/images/contact/background.png';
import iconPhone from '~/images/contact/iconPhone.png';
import iconEmail from '~/images/contact/iconEmail.png';

import checkBox from '~/images/icons/checkbox.png';

import {
  ScrollView,
  Container,
  BoxContent,
  Text,
  Infos,
  InfoRow,
  Icon,
  InfosText,
  Form,
  Row,
  Label,
  InputText,
  PickerContainer,
  Picker,
  RowColumn,
  Column,
  RowTerms,
  CheckboxBackground,
  TermsText,
  ButtonSubmit,
  ButtonSubmitText,
} from './styles';

const Contact = ({ navigation }) => (
  <Container source={background}>
    <Header title="Contato" navigation={navigation} historyBack="Main" />
    <ScrollView>
      <BoxContent>
        <Text>
          A Turma do Parque da Mônica está preparada para garantir a diversão de toda a sua família!
        </Text>
        <Text>
          Em caso de dúvidas, você pode entrar em contato através dos seguintes canais de
          atendimento:
        </Text>
        <Infos>
          <InfoRow>
            <Icon source={iconPhone} />
            <InfosText>CENTRAL TELEFÔNICA: (11) 5693-2200</InfosText>
          </InfoRow>
          <InfoRow>
            <Icon source={iconEmail} />
            <InfosText>sav@parquedamonica.com.br</InfosText>
          </InfoRow>
        </Infos>
        <Text>Ou preencha o formulário abaixo!</Text>
        <Text>Entraremos em contato o mais breve possível.</Text>
        <Form>
          <Row>
            <Label>Nome Completo</Label>
            <InputText placeholder="Pedro da Silva" />
          </Row>
          <Row>
            <Label>E-mail</Label>
            <InputText placeholder="pedroca@pdm.com.br" />
          </Row>
          <Row>
            <Label>Telefone</Label>
            <InputText placeholder="011 12345-6789" />
          </Row>
          <Row>
            <Label>Cidade</Label>
            <RowColumn>
              <Column width="55%">
                <InputText placeholder="São Paulo" />
              </Column>
              <Column width="35%">
                <PickerContainer>
                  <Picker>
                    <Picker.Item label="SP" value="SP" color="#a4a4a4" />
                    <Picker.Item label="RJ" value="RJ" color="#a4a4a4" />
                  </Picker>
                </PickerContainer>
              </Column>
            </RowColumn>
          </Row>
          <Row>
            <Label>Assunto</Label>
            <PickerContainer>
              <Picker>
                <Picker.Item label="Assunto1" value="Assunto1" color="#a4a4a4" />
                <Picker.Item label="Assunto2" value="Assunto2" color="#a4a4a4" />
              </Picker>
            </PickerContainer>
          </Row>
          <Row>
            <InputText multiline numberOfLines={4} placeholder="Digite sua mensagem aqui" />
          </Row>
          <Row center>
            <RowTerms>
              <CheckboxBackground source={checkBox} />
              <TermsText>
                By creating an account you agree to our Terms of Service and Privacy Policy
              </TermsText>
            </RowTerms>
          </Row>
          <Row center>
            <ButtonSubmit>
              <ButtonSubmitText>ENVIAR</ButtonSubmitText>
            </ButtonSubmit>
          </Row>
        </Form>
      </BoxContent>
    </ScrollView>
    <Footer navigation={navigation} />
  </Container>
);

Contact.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default Contact;
