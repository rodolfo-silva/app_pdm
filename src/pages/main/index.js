/* eslint-disable react-native/no-raw-text */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { ScrollView } from 'react-native';

import {
  Container, Text, Button, ButtonText,
} from './styles';

class Main extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  goPage = (page) => {
    const { navigation } = this.props;
    navigation.navigate(page);
  };

  render() {
    return (
      <ScrollView>
        <Container>
          <Text>Navegação direta das páginas</Text>
          <Button
            onPress={() => {
              this.goPage('CreateAccount');
            }}
          >
            <ButtonText>Criar conta</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Login');
            }}
          >
            <ButtonText>Login</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('ForgotPassword');
            }}
          >
            <ButtonText>Esqueci minha senha</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Introduction');
            }}
          >
            <ButtonText>Introdução do App</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Home');
            }}
          >
            <ButtonText>Home</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Programming');
            }}
          >
            <ButtonText>Programação</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Calendar');
            }}
          >
            <ButtonText>Calendário</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Attractions');
            }}
          >
            <ButtonText>Atrações</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('AttractionsDetail');
            }}
          >
            <ButtonText>Atrações Detalhe</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Map');
            }}
          >
            <ButtonText>Mapa e Tempo</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Script');
            }}
          >
            <ButtonText>Montando seu Roteiro</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Treasure');
            }}
          >
            <ButtonText>Caça ao Tesouro</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Achievements');
            }}
          >
            <ButtonText>Atrações conquistadas</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Profile');
            }}
          >
            <ButtonText>Perfil</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Location');
            }}
          >
            <ButtonText>Localização</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Services');
            }}
          >
            <ButtonText>Serviços</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Faq');
            }}
          >
            <ButtonText>Faq</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Contact');
            }}
          >
            <ButtonText>Contato</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Terms');
            }}
          >
            <ButtonText>Termos de uso</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Videolibrary');
            }}
          >
            <ButtonText>Videoteca</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('VideolibraryDetail');
            }}
          >
            <ButtonText>Videoteca Detalhe</ButtonText>
          </Button>
          <Button
            onPress={() => {
              this.goPage('Scancode');
            }}
          >
            <ButtonText>Scanear QR Code</ButtonText>
          </Button>
        </Container>
      </ScrollView>
    );
  }
}

export default Main;
