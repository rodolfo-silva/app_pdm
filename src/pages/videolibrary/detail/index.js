import React from 'react';
import PropTypes from 'prop-types';

import Header from '~/components/Header';
import Footer from '~/components/Footer';

import {
  Container,
  ScrollView,
  Video,
  Center,
  Infos,
  Row,
  Title,
  Time,
  Description,
  MoreVideos,
  MoreVideosTitle,
  RowVideo,
  Thumb,
  VideoInfo,
  VideoInfoTitle,
  VideoInfoTime,
} from './styles';

import video from '~/images/videoLibrary/detail/video.png';
import moreVideos1 from '~/images/videoLibrary/detail/moreVideos1.png';
import moreVideos2 from '~/images/videoLibrary/detail/moreVideos2.png';

const VideolibraryDetail = ({ navigation }) => (
  <Container>
    <Header title="Videoteca" navigation={navigation} historyBack="Videolibrary" />
    <ScrollView>
      <Video source={video} />
      <Center>
        <Infos>
          <Row>
            <Title>Cadê o Sansão?</Title>
            <Time>26:34</Time>
          </Row>
          <Row>
            <Description>
              O desaparecimento do Sansão vira notícia no Parque da Mônica. E então começa um novo
              espetáculo com muita diversão para toda a família.
            </Description>
          </Row>
        </Infos>
        <MoreVideos>
          <MoreVideosTitle>Assista mais vídeos</MoreVideosTitle>
          <RowVideo>
            <Thumb source={moreVideos1} />
            <VideoInfo>
              <VideoInfoTitle>A Fábrica de Brinquedos do Papai Noel</VideoInfoTitle>
              <VideoInfoTime>45 min</VideoInfoTime>
            </VideoInfo>
          </RowVideo>
          <RowVideo last>
            <Thumb source={moreVideos2} />
            <VideoInfo>
              <VideoInfoTitle>Viajando nos Livros</VideoInfoTitle>
              <VideoInfoTime>45 min</VideoInfoTime>
            </VideoInfo>
          </RowVideo>
        </MoreVideos>
      </Center>
    </ScrollView>
    <Footer navigation={navigation} />
  </Container>
);

VideolibraryDetail.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default VideolibraryDetail;
