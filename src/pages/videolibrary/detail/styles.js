import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background: #00517d;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
`;

export const Video = styled.Image``;

export const Center = styled.View`
  flex-direction: column;
  padding: 20px 5%;
`;

export const Infos = styled.View`
  flex-direction: column;
`;

export const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: baseline;
  margin-bottom: 10px;
`;

export const Title = styled.Text`
  color: #ffe602;
  font-size: 20px;
  font-weight: 400;
`;

export const Time = styled.Text`
  color: #a4a4a4;
  font-size: 14px;
  font-style: italic;
`;

export const Description = styled.Text`
  color: #ffffff;
  font-size: 14px;
  font-weight: 700;
  line-height: 20px;
`;

export const MoreVideos = styled.View`
  background: #ffffff;
  border-radius: 5px;
  padding: 20px 15px 30px;
  margin-top: 20px;
  flex-direction: column;
`;

export const MoreVideosTitle = styled.Text`
  color: #ed2626;
  font-size: 16px;
  font-weight: 400;
  margin-bottom: 10px;
`;

export const RowVideo = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding-top: 20px;
  padding-bottom: ${props => (props.last ? '0' : '20px')};
  border-bottom-width: ${props => (props.last ? '0' : '1px')};
  border-bottom-color: #ccc;
`;

export const Thumb = styled.Image``;

export const VideoInfo = styled.View`
  flex-direction: column;
  justify-content: space-between;
  flex-wrap: wrap;
  width: 40%;
`;

export const VideoInfoTitle = styled.Text`
  color: #363636;
  font-size: 16px;
  font-weight: 700;
`;

export const VideoInfoTime = styled.Text`
  color: #a4a4a4;
  font-size: 14px;
  font-style: italic;
`;
