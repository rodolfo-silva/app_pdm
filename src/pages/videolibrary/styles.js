import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background: #00517d;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
  margin-top: -26px;
`;

export const Banner = styled.Image``;

export const Center = styled.View`
  flex-direction: column;
  padding: 20px 5% 10px;
  align-items: center;
`;

export const Label = styled.Text`
  color: #ffffff;
  font-size: 16px;
  font-weight: 700;
  margin-bottom: 20px;
  text-align: center;
  width: 90%;
`;

export const ListVideos = styled.View``;

export const Video = styled.Image`
  margin-bottom: 10px;
`;
