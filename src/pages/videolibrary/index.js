import React from 'react';
import PropTypes from 'prop-types';

import { TouchableWithoutFeedback } from 'react-native';

import Header from '~/components/Header';
import Footer from '~/components/Footer';

import {
  Container,
  ScrollView,
  Banner,
  Center,
  Label,
  ListVideos,
  Video,
} from './styles';

import banner from '~/images/videoLibrary/banner.png';
import video1 from '~/images/videoLibrary/video1.png';
import video2 from '~/images/videoLibrary/video2.png';
import video3 from '~/images/videoLibrary/video3.png';

const Videolibrary = ({ navigation }) => (
  <Container>
    <Header title="Videoteca" navigation={navigation} historyBack="Main" />
    <ScrollView>
      <Banner source={banner} />
      <Center>
        <Label>Assista os nossos musicais e divirta-se com todas as histórias!</Label>
        <ListVideos>
          <TouchableWithoutFeedback onPress={() => { navigation.navigate('VideolibraryDetail'); }}>
            <Video source={video1} />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => { navigation.navigate('VideolibraryDetail'); }}>
            <Video source={video2} />
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={() => { navigation.navigate('VideolibraryDetail'); }}>
            <Video source={video3} />
          </TouchableWithoutFeedback>
        </ListVideos>
      </Center>
    </ScrollView>
    <Footer navigation={navigation} />
  </Container>
);

Videolibrary.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default Videolibrary;
