import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { View, Dimensions, Image } from 'react-native';

import Carousel, { Pagination } from 'react-native-snap-carousel';

import {
  Container,
  ScrollView,
  BoxCarousel,
  Center,
  Content,
  Row,
  SubRow,
  Title,
  Description,
  TitleSection,
  Icon,
  Text,
  TextRestriction,
  List,
  Item,
} from './styles';

import Header from '~/components/Header';
import Footer from '~/components/Footer';

import background from '~/images/attractions/detail/background.png';
import banner from '~/images/attractions/detail/banner.png';
import icon1 from '~/images/attractions/detail/icon1.png';
import icon2 from '~/images/attractions/detail/icon2.png';

const { width: viewportWidth } = Dimensions.get('window');

class AttractionsDetail extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  state = {
    activeSlide: 0,
    slides: [
      {
        thumbnail: banner,
      },
      {
        thumbnail: banner,
      },
      {
        thumbnail: banner,
      },
    ],
  };

  get pagination() {
    const { slides, activeSlide } = this.state;
    return (
      <Pagination
        containerStyle={{ marginTop: -60 }}
        dotsLength={slides.length}
        activeDotIndex={activeSlide}
        dotStyle={{
          width: 7,
          height: 7,
          borderRadius: 10,
          marginHorizontal: 1,
          backgroundColor: '#ed2626',
        }}
        inactiveDotStyle={{
          backgroundColor: '#0572ba',
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  }

  handleSnapToItem(index) {
    this.setState({ activeSlide: index });
  }

  renderItem({ item, index }) {
    return (
      <View>
        <Image source={banner} />
      </View>
    );
  }

  render() {
    const { navigation } = this.props;
    const { slides } = this.state;

    return (
      <Container source={background}>
        <Header title="Atrações" navigation={navigation} historyBack="Attractions" />
        <ScrollView>
          <BoxCarousel>
            <Carousel
              ref={(c) => {
                this._carousel = c;
              }}
              data={slides}
              renderItem={this.renderItem}
              sliderWidth={viewportWidth}
              itemWidth={viewportWidth}
              onSnapToItem={(index) => {
                this.handleSnapToItem(index);
              }}
            />
            {this.pagination}
          </BoxCarousel>
          <Center>
            <Content>
              <Row>
                <Title>coelhada nas estrelas</Title>
                <Description>
                  Um super simulador 4D que vai levar toda a família em uma viagem no espaço cheia
                  de surpresas!
                </Description>
              </Row>
              <Row bg="#fcf390">
                <SubRow>
                  <TitleSection>Restrições</TitleSection>
                  <Text>Segurança deixa a brincadeira ainda mais divertida.</Text>
                  <Text>Leia com atenção as informações abaixo:</Text>
                </SubRow>
              </Row>
              <Row>
                <SubRow>
                  <List>
                    <Item>
                      <Icon source={icon1} resizeMode="contain" />
                      <TextRestriction>Altura Mínima 1,00 metro</TextRestriction>
                    </Item>
                    <Item>
                      <Icon source={icon2} resizeMode="contain" />
                      <TextRestriction>Capacidade Máxima 48 Visitantes</TextRestriction>
                    </Item>
                  </List>
                </SubRow>
              </Row>
              <Row bg="#f3f3f3">
                <SubRow>
                  <TitleSection>Não é permitido</TitleSection>
                  <List>
                    <Item>
                      <Text>- Alimentos e bebidas</Text>
                    </Item>
                    <Item>
                      <Text>- Gesso ou prótese em membros inferiores e superiores</Text>
                    </Item>
                    <Item>
                      <Text>- Gestantes</Text>
                    </Item>
                    <Item>
                      <Text>- Bastões de selfie</Text>
                    </Item>
                    <Item>
                      <Text>- Fotografar ou filmar</Text>
                    </Item>
                  </List>
                </SubRow>
              </Row>
              <Row>
                <SubRow>
                  <TitleSection>Não recomendado</TitleSection>
                  <List>
                    <Item>
                      <Text>- Portadores de cardiopatia</Text>
                    </Item>
                    <Item>
                      <Text>- Portadores de labirintite e epilepsia</Text>
                    </Item>
                    <Item>
                      <Text>- Problemas na coluna</Text>
                    </Item>
                    <Item>
                      <Text>- Fobias específicas</Text>
                    </Item>
                  </List>
                </SubRow>
              </Row>
              <Row bg="#f3f3f3">
                <SubRow>
                  <TitleSection>Recomendações de Segurança</TitleSection>
                  <Text marginBottom>- Em casos de pessoas com deficiência, dirija-se ao SAV (Serviço de Atendimento ao Visitante) e retire seu guia de orientações de segurança.</Text>
                  <Text marginBottom>- Não é permitido guardar ou furar lugares na fila.</Text>
                  <Text marginBottom>- Caso haja necessidade emergencial de sair da fila, informe um integrante da nossa equipe.</Text>
                  <Text marginBottom>- Não nos reponsabilizamos por objetos esquecidos ou danificados nesta atração.</Text>
                  <Text marginBottom>- Oriente crianças a não pular, subir ou escalar as cercas da fila desta atração.</Text>
                  <Text marginBottom>- Siga as orientações dos integrantes da nossa equipe a todo momento.</Text>
                  <Text marginBottom>- Nossa equipe está orientada a garantir o cumprimento de todas as normas de segurança.</Text>
                  <Text marginBottom>- Nesta atração você poderá se molhar.</Text>
                </SubRow>
              </Row>
            </Content>
          </Center>
        </ScrollView>
        <Footer navigation={navigation} />
      </Container>
    );
  }
}

export default AttractionsDetail;
