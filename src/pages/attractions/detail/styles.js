import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
  margin-top: -26px;
`;

export const BoxCarousel = styled.View`
  height: 262px;
`;

export const Center = styled.View`
  flex-direction: row;
  justify-content: center;
`;

export const Content = styled.View`
  flex-direction: column;
  background: #fff;
  border-radius: 6px;
  width: 90%;
  padding: 20px 0 10px;
  margin: 20px 0 26px;
`;

export const Row = styled.View`
  flex-direction: column;
  align-items: center;
  padding: 0 20px;
  background: ${props => (props.bg ? props.bg : '#fff')};
`;

export const Icon = styled.ImageBackground`
  width: 25%;
  height: 27px;
`;

export const Title = styled.Text`
  color: #0572ba;
  font-size: 24px;
  font-weight: 400;
`;

export const Description = styled.Text`
  color: #363636;
  font-size: 14px;
  font-weight: 700;
  line-height: 18px;
  margin: 20px 0 25px;
`;

export const SubRow = styled.View`
  width: 100%;
  padding: 15px 0;
  flex-direction: column;
`;

export const TitleSection = styled.Text`
  color: #ed2626;
  font-size: 13px;
  font-weight: 700;
  margin-bottom: 10px;
`;

export const Text = styled.Text`
  color: #363636;
  font-size: 11px;
  font-weight: 700;
  line-height: 15px;
  margin-bottom: ${props => (props.marginBottom ? '10px' : '0')};
`;

export const TextRestriction = styled.Text`
  color: #363636;
  font-size: 10px;
  font-weight: 700;
  line-height: 14px;
  width: 70%;
`;

export const List = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
  align-content: flex-start;
`;

export const Item = styled.View`
  width: 48%;
  margin-bottom: 5px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
