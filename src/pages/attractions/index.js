/* eslint-disable react-native/no-raw-text */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { TouchableWithoutFeedback } from 'react-native';

import shortHeader from '~/images/shortHeader.png';
import background from '~/images/attractions/background.png';
import itemBackground from '~/images/attractions/itemBackground.png';
import iconArrow from '~/images/attractions/iconArrow.png';

import {
  Container,
  ScrollView,
  ShortHeader,
  Center,
  Grid,
  BoxFilter,
  Title,
  Description,
  PickerContainer,
  Picker,
  List,
  Item,
  Icon,
} from './styles';

import Footer from '~/components/Footer';

class Attractions extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  goPage = (page) => {
    const { navigation } = this.props;
    navigation.navigate(page);
  };

  render() {
    const { navigation } = this.props;
    return (
      <Container source={background}>
        <ShortHeader source={shortHeader} />
        <ScrollView>
          <Center>
            <Grid>
              <BoxFilter>
                <Title>fique por dentro das atrações</Title>
                <Description>Faça o filtro por altura mínima recomendada:</Description>
                <PickerContainer>
                  <Picker>
                    <Picker.Item label="Altura mínima: 1,40" value="Altura mínima: 1,40" />
                  </Picker>
                </PickerContainer>
              </BoxFilter>
              <List>
                <TouchableWithoutFeedback onPress={() => { navigation.navigate('AttractionsDetail'); }}>
                  <Item source={itemBackground}>
                    <Icon source={iconArrow} />
                  </Item>
                </TouchableWithoutFeedback>
              </List>
            </Grid>
          </Center>
        </ScrollView>
        <Footer navigation={navigation} />
      </Container>
    );
  }
}

export default Attractions;
