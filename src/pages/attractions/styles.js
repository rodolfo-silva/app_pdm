import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
`;

export const ShortHeader = styled.ImageBackground`
  width: 100%;
  height: 99px;
  z-index: 1;
`;

export const Center = styled.View`
  flex: 1;
  align-items: center;
  margin-top: 20px;
`;

export const Grid = styled.View`
  width: 90%;
`;

export const BoxFilter = styled.View`
  background: #0572ba;
  flex-direction: column;
  align-items: center;
  padding: 20px 20px 30px;
  border-top-left-radius: 6px;
  border-top-right-radius: 6px;
`;

export const Title = styled.Text`
  width: 70%;
  color: #fbe926;
  font-size: 22px;
  font-weight: 400;
  text-transform: uppercase;
  text-align: center;
  margin-bottom: 20px;
`;

export const Description = styled.Text`
  color: #ffffff;
  font-size: 13px;
  font-weight: 700;
  margin-bottom: 20px;
`;

export const PickerContainer = styled.View`
  width: 100%;
  border-radius: 9px;
  border: 1px solid #2f658e;
  padding: 5px;
  background: #fff;
`;

export const Picker = styled.Picker`
  width: 100%;
  height: 40px;
  border-radius: 9px;
  font-weight: 700;
`;

export const List = styled.View`
  margin-top: 15px;
`;

export const Item = styled.ImageBackground`
  height: 82px;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  padding: 0 20px;
`;
export const Icon = styled.Image``;
