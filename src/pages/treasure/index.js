/* eslint-disable react-native/no-raw-text */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  Container, Text, Button, ButtonText,
} from './styles';

class Treasure extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  goPage = (page) => {
    const { navigation } = this.props;
    navigation.navigate(page);
  };

  render() {
    return (
      <Container>
        <Text>Caça ao Tesouro</Text>
        <Button
          onPress={() => {
            this.goPage('Main');
          }}
        >
          <ButtonText>Voltar para o início</ButtonText>
        </Button>
      </Container>
    );
  }
}

export default Treasure;
