/* eslint-disable react-native/no-raw-text */
import React from 'react';
import PropTypes from 'prop-types';

import Header from '~/components/Header';
import Footer from '~/components/Footer';

import background from '~/images/faq/background.png';
import iconPlus from '~/images/faq/iconPlus.png';

import {
  Container,
  ScrollView,
  List,
  Item,
  ContentTitle,
  BoxTitle,
  Title,
  Number,
  Icon,
  Content,
  ContentText,
} from './styles';

const Faq = ({ navigation }) => (
  <Container source={background}>
    <Header title="Faq" navigation={navigation} historyBack="Main" />
    <ScrollView>
      <List>
        <Item>
          <ContentTitle odd first>
            <BoxTitle>
              <Title>
                <Number>1.</Number>
                {' '}
ONDE POSSO COMPRAR OS INGRESSOS PARA O PARQUE DA MÔNICA?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
          <Content>
            <ContentText>
              Você pode adquirir os ingressos para o Parque da Mônica através do
              https://ecommerce.parquedamonica.com.br/ além de garantir sua visita na data desejada,
              você ganha 10% de desconto no valor dos ingressos, porém é necessário comprar com ao
              menos um dia de antecedência da data do passeio. O pagamento no e-commerce pode ser
              realizado somente em cartões de crédito, em até 5x sem juros nas bandeiras Visa,
              Mastercard e Amex.
            </ContentText>
            <ContentText>
              Todos os adultos acompanhados de crianças (até 17 anos), menores de 18 anos, idosos,
              acompanhantes de pessoas com deficiência e estudantes tem direito a meia entrada no
              Parque da Mônica.
            </ContentText>
            <ContentText>
              Você também pode comprar seus ingressos na bilheteria do Parque da Mônica, que abre 30
              minutos antes da abertura e fecha 30 minutos antes do encerramento do Parque. O
              pagamento na bilheteria é facilitado em até 5x sem juros nos cartões Visa, Mastercard,
              Hipercard ou Diners Club, Amex, Sorocred, Elo Cred e cred, e à vista em dinheiro ou
              débito. Não aceitamos cheque.
            </ContentText>
            <ContentText>
              Crianças de até 1 e 11 meses possuem ingresso gratuito no Parque da Mônica, você pode
              adquirir esse ingresso através do e-commerce do Parque mediante a compra de um
              ingresso pagante e, apresentar no dia de sua visita o documento do menor na entrada do
              Parque da Mônica.
            </ContentText>
            <ContentText>
              Crianças com deficiência (de 2 a 12 anos) possuem ingresso gratuito no Parque da
              Mônica, basta apresentar documentação comprobatória e retirar o ingresso na Bilheteria
              no dia de sua visita.
            </ContentText>
            <ContentText>Ingresso para gestantes tem o valor de R$50,00.</ContentText>
          </Content>
        </Item>
        <Item>
          <ContentTitle>
            <BoxTitle>
              <Title>
                <Number>2.</Number>
                {' '}
QUAL É O PREÇO E QUAIS AS CONDIÇÕES DE PAGAMENTO DOS INGRESSOS?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle odd>
            <BoxTitle>
              <Title>
                <Number>3.</Number>
                {' '}
ESTRUTURA DO PARQUE
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle>
            <BoxTitle>
              <Title>
                <Number>4.</Number>
                {' '}
EXISTE PROMOÇÃO PARA ANIVERSARIANTES?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle odd>
            <BoxTitle>
              <Title>
                <Number>5.</Number>
                {' '}
COMO FAÇO PARA REALIZAR FESTA DE ANIVERSÁRIO NO PARQUE DA
                MÔNICA?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle>
            <BoxTitle>
              <Title>
                <Number>6.</Number>
                {' '}
O PARQUE DA MÔNICA ACEITA MEIA-ENTRADA?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle odd>
            <BoxTitle>
              <Title>
                <Number>7.</Number>
                {' '}
ONDE O PARQUE DA MÔNICA ESTÁ LOCALIZADO?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle>
            <BoxTitle>
              <Title>
                <Number>8.</Number>
                {' '}
POSSO BRINCAR EM TODAS AS ATRAÇÕES?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle odd>
            <BoxTitle>
              <Title>
                <Number>9.</Number>
                {' '}
PESSOAS COM DEFICIÊNCIA TAMBÉM PODEM BRINCAR EM TODAS AS
                ATRAÇÕES?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle>
            <BoxTitle>
              <Title>
                <Number>10.</Number>
                {' '}
COMO FUNCIONA A ALIMENTAÇÃO NO PARQUE DA MÔNICA?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle odd>
            <BoxTitle>
              <Title>
                <Number>11.</Number>
                {' '}
QUAIS SÃO AS ATRAÇÕES E SERVIÇOS PAGOS À PARTE?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle>
            <BoxTitle>
              <Title>
                <Number>12.</Number>
                {' '}
COMO REALIZO EXCURSÕES AO PARQUE DA MÔNICA?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle odd>
            <BoxTitle>
              <Title>
                <Number>13.</Number>
                {' '}
QUAIS SÃO OS DIAS E HORÁRIOS DE FUNCIONAMENTO DO PARQUE DA
                MÔNICA?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle>
            <BoxTitle>
              <Title>
                <Number>14.</Number>
                {' '}
EXISTE INGRESSO FAMÍLIA?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle odd>
            <BoxTitle>
              <Title>
                <Number>15.</Number>
                {' '}
ENCONTRAREI ALGUMA ATRAÇÃO FORA DE OPERAÇÃO NO DIA DA MINHA
                VISITA?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle>
            <BoxTitle>
              <Title>
                <Number>16.</Number>
                {' '}
QUAIS ATRAÇÕES ESTÃO INCLUSAS NO VALOR DO INGRESSO DE ENTRADA?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle odd>
            <BoxTitle>
              <Title>
                <Number>17.</Number>
                {' '}
MEU INGRESSO VENCEU. POSSO REVALIDÁ-LO?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle>
            <BoxTitle>
              <Title>
                <Number>18.</Number>
                {' '}
POSSO ENTRAR COM ALIMENTOS E BEBIDAS NO PARQUE DA MÔNICA?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
        <Item>
          <ContentTitle last odd>
            <BoxTitle>
              <Title>
                <Number>19.</Number>
                {' '}
PROFESSORES PAGAM MEIA-ENTRADA?
              </Title>
            </BoxTitle>
            <Icon source={iconPlus} />
          </ContentTitle>
        </Item>
      </List>
    </ScrollView>
    <Footer navigation={navigation} />
  </Container>
);

Faq.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default Faq;
