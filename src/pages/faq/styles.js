import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
  flex-direction: column;
  padding: 15px;
`;

export const List = styled.View`
  margin-bottom: 40px;
`;

export const Item = styled.View``;

export const ContentTitle = styled.View`
  background-color: ${props => (props.odd ? '#70dcff' : '#91e4ff')};
  padding: 15px 20px;
  flex-direction: row;
  justify-content: space-between;
  border-top-left-radius: ${props => (props.first ? '5px' : '0')};
  border-top-right-radius: ${props => (props.first ? '5px' : '0')};
  border-bottom-left-radius: ${props => (props.last ? '5px' : '0')};
  border-bottom-right-radius: ${props => (props.last ? '5px' : '0')};
`;

export const BoxTitle = styled.View`
  width: 80%;
`;

export const Title = styled.Text`
  color: #363636;
  font-size: 11px;
  font-weight: 700;
  line-height: 17px;
  text-transform: uppercase;
`;

export const Number = styled.Text`
  color: #c02125;
`;

export const Icon = styled.Image``;

export const Content = styled.View`
  background: #fff;
  padding: 20px;
`;

export const ContentText = styled.Text`
  color: #363636;
  font-size: 10px;
  font-weight: 700;
  line-height: 13px;
  margin-bottom: 15px;
`;
