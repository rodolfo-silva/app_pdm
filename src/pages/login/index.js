/* eslint-disable react-native/no-raw-text */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import header from '~/images/header.png';
import background from '~/images/background.png';
import iconFacebook from '~/images/icons/iconFacebook.png';

import { TouchableWithoutFeedback } from 'react-native';

import {
  Container,
  ScrollView,
  Header,
  Form,
  LabelLogin,
  AnchorSign,
  LabelField,
  TextInput,
  LabelOr,
  ButtonLogin,
  IconFacebook,
  ButtonLoginText,
  LabelForgotPassword,
} from './styles';

class Login extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  goPage = (page) => {
    const { navigation } = this.props;
    navigation.navigate(page);
  };

  render() {
    return (
      <Container source={background}>
        <ScrollView>
          <Header source={header} />
          <Form>
            <LabelLogin>
              Faça o login ou
              {' '}
              <TouchableWithoutFeedback onPress={() => { this.goPage('CreateAccount'); }}>
                <AnchorSign>Crie sua conta</AnchorSign>
              </TouchableWithoutFeedback>
            </LabelLogin>
            <LabelField>Email</LabelField>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              underlineColorAndroid="transparent"
            />
            <LabelField>Senha</LabelField>
            <TextInput
              autoCapitalize="none"
              autoCorrect={false}
              secureTextEntry
              underlineColorAndroid="transparent"
            />
            <ButtonLogin
              onPress={() => {
                this.goPage('Home');
              }}
            >
              <ButtonLoginText>entrar</ButtonLoginText>
            </ButtonLogin>
            <LabelOr>ou</LabelOr>
            <ButtonLogin
              facebook
              onPress={() => {
                this.goPage('Home');
              }}
            >
              <IconFacebook source={iconFacebook} />
              <ButtonLoginText>entrar com o facebook</ButtonLoginText>
            </ButtonLogin>

            <TouchableWithoutFeedback
              onPress={() => {
                this.goPage('ForgotPassword');
              }}
            >
              <LabelForgotPassword>Esqueceu a senha?</LabelForgotPassword>
            </TouchableWithoutFeedback>
          </Form>
        </ScrollView>
      </Container>
    );
  }
}

export default Login;
