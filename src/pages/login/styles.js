import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
`;

export const Header = styled.ImageBackground`
  width: 100%;
  height: 181px;
  margin-bottom: 20px;
`;

export const Form = styled.View`
  justify-content: center;
  padding: 0 8%;
`;

export const LabelLogin = styled.Text`
  text-align: center;
  font-size: 16px;
  font-weight: 700;
  color: #363636;
  margin: 10px 0 20px;
`;

export const AnchorSign = styled.Text`
  text-decoration: underline;
  color: #e6423e;
  font-size: 16px;
  font-weight: 700;
`;

export const LabelField = styled.Text`
  color: #363636;
  font-size: 14px;
  font-weight: 700;
  margin-bottom: 5px;
`;

export const TextInput = styled.TextInput`
  width: 100%;
  height: 48px;
  border-radius: 6px;
  border: 1px solid #2173a1;
  background-color: #ffffff;
  margin-bottom: 20px;
`;

export const ButtonLogin = styled.TouchableOpacity`
  width: 100%;
  height: 48px;
  text-align: center;
  background: ${props => (props.facebook ? '#0572ba' : '#ed2626')};
  border-radius: 4px;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  margin: 10px 0;
`;

export const IconFacebook = styled.Image``;

export const ButtonLoginText = styled.TextInput`
  color: #fff;
  font-size: 16px;
  font-weight: bold;
  text-transform: uppercase;
  margin-left: 10px;
`;

export const LabelOr = styled.Text`
  text-align: center;
  font-size: 16px;
  font-weight: 400;
  color: #363636;
`;

export const LabelForgotPassword = styled.Text`
  text-align: center;
  font-size: 16px;
  font-weight: 700;
  color: #363636;
  text-decoration: underline;
  margin: 15px 0 30px;
`;
