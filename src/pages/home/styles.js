import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background: #f2f2f2;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
  margin-top: -22px;
`;

export const ShortHeader = styled.ImageBackground`
  width: 100%;
  height: 99px;
  z-index: 1;
`;

export const CenterContent = styled.View`
  flex: 1;
  align-items: center;
`;

export const BoxCalendarProgramming = styled.View`
  width: 90%;
  flex-direction: row;
`;

export const BoxCalendar = styled.ImageBackground`
  width: 50%;
  height: 170px;
`;

export const BoxProgramming = styled.ImageBackground`
  width: 50%;
  height: 170px;
`;

export const BoxItemNavigation = styled.ImageBackground`
  width: 92%;
  height: ${props => (props.last ? '260px' : '200px')};
  margin-bottom: ${props => (props.last ? '-60px' : '0')};
`;
