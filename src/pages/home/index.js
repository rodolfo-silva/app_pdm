/* eslint-disable react-native/no-raw-text */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { TouchableWithoutFeedback } from 'react-native';

import shortHeader from '~/images/shortHeader.png';
import backgroundCalendar from '~/images/home/backgroundCalendar.png';
import backgroundProgramming from '~/images/home/backgroundProgramming.png';
import backgroundAttractions from '~/images/home/backgroundAttractions.png';
import backgroundMap from '~/images/home/backgroundMap.png';
import backgroundScript from '~/images/home/backgroundScript.png';
import backgroundTreasure from '~/images/home/backgroundTreasure.png';
import backgroundMyAttractions from '~/images/home/backgroundMyAttractions.png';

import {
  Container,
  ScrollView,
  ShortHeader,
  CenterContent,
  BoxCalendarProgramming,
  BoxCalendar,
  BoxProgramming,
  BoxItemNavigation,
} from './styles';

import Footer from '~/components/Footer';

class Home extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  goPage = (page) => {
    const { navigation } = this.props;
    navigation.navigate(page);
  };

  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <ShortHeader source={shortHeader} />
        <ScrollView>
          <CenterContent>
            <BoxCalendarProgramming>
              <TouchableWithoutFeedback
                onPress={() => {
                  navigation.navigate('Calendar');
                }}
              >
                <BoxCalendar source={backgroundCalendar} />
              </TouchableWithoutFeedback>

              <TouchableWithoutFeedback
                onPress={() => {
                  navigation.navigate('Programming');
                }}
              >
                <BoxProgramming source={backgroundProgramming} />
              </TouchableWithoutFeedback>
            </BoxCalendarProgramming>

            <TouchableWithoutFeedback
              onPress={() => {
                navigation.navigate('AttractionsDetail');
              }}
            >
              <BoxItemNavigation source={backgroundAttractions} resizeMode="contain" />
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
              onPress={() => {
                navigation.navigate('Map');
              }}
            >
              <BoxItemNavigation source={backgroundMap} resizeMode="contain" />
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
              onPress={() => {
                navigation.navigate('Script');
              }}
            >
              <BoxItemNavigation source={backgroundScript} resizeMode="contain" />
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
              onPress={() => {
                navigation.navigate('Treasure');
              }}
            >
              <BoxItemNavigation source={backgroundTreasure} resizeMode="contain" />
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
              onPress={() => {
                navigation.navigate('Achievements');
              }}
            >
              <BoxItemNavigation source={backgroundMyAttractions} resizeMode="contain" last />
            </TouchableWithoutFeedback>
          </CenterContent>
        </ScrollView>
        <Footer navigation={navigation} />
      </Container>
    );
  }
}

export default Home;
