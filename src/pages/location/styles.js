import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
  flex-direction: column;
  padding: 15px;
`;

export const HowToGet = styled.View`
  border-radius: 5px;
  background-color: #ffe602;
  padding: 20px 10px;
  flex-direction: column;
  align-items: center;
`;

export const Title = styled.Text`
  color: #0572ba;
  font-size: 18px;
  font-weight: 400;
  margin-bottom: 20px;
`;

export const Text = styled.Text`
  color: #363636;
  font-size: 13px;
  font-weight: 700;
  line-height: 17px;
  text-align: center;
  margin-bottom: 10px;
`;

export const Label = styled.Text`
  color: #363636;
  font-size: 10px;
  font-weight: 700;
  margin: 20px 0;
`;

export const BoxSearch = styled.View`
  width: 100%;
  padding: 10px 15px;
  border-radius: 9px;
  border: 1px solid #ffcc02;
  background-color: #ffffff;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const TextInput = styled.TextInput.attrs({
  placeholderTextColor: '#363636',
  fontWeight: 'bold',
})`
  width: 90%;
  height: 30px;
  line-height: 30px;
  font-size: 13px;
  padding: 0 10px;
`;

export const Icon = styled.Image``;

export const Map = styled.View`
  width: 100%;
  height: 271px;
  border-radius: 5px;
  background-color: #0572ba;
  margin: 20px 0 40px;
`;

export const Image = styled.Image``;
