/* eslint-disable react-native/no-raw-text */
import React from 'react';
import PropTypes from 'prop-types';

import Header from '~/components/Header';
import Footer from '~/components/Footer';

import background from '~/images/location/background.png';
import iconSearch from '~/images/location/iconSearch.png';
import map from '~/images/location/map.png';

import {
  ScrollView,
  Container,
  HowToGet,
  Title,
  Text,
  Label,
  BoxSearch,
  TextInput,
  Icon,
  Map,
  Image,
} from './styles';

const Location = ({ navigation }) => (
  <Container source={background}>
    <Header title="Localização" navigation={navigation} historyBack="Main" />
    <ScrollView>
      <HowToGet>
        <Title>Como chegar</Title>
        <Text>O Parque da Mônica está localizado no Shopping SP Market.</Text>
        <Text>Av. das Nações Unidas, 22.540 - Cep:04795-000</Text>
        <Label>Abaixo, você pode conferir como chegar:</Label>
        <BoxSearch>
          <TextInput placeholder="Digite sua localização" />
          <Icon source={iconSearch} />
        </BoxSearch>
      </HowToGet>
      <Map>
        <Image source={map} />
      </Map>
    </ScrollView>
    <Footer navigation={navigation} />
  </Container>
);

Location.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default Location;
