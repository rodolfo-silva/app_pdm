/* eslint-disable react-native/no-raw-text */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { TouchableWithoutFeedback } from 'react-native';

import shortHeader from '~/images/shortHeader.png';
import background from '~/images/attractions/background.png';
import itemBackground from '~/images/attractions/itemBackground.png';
import select from '~/images/icons/select.png';
import selected from '~/images/icons/selected.png';

import backgroundRoteiro from '~/images/script/bg_roteiro.png';

import {
  Container,
  ScrollView,
  ShortHeader,
  Center,
  Grid,
  BoxFilter,
  Title,
  Description,
  PickerContainer,
  Picker,
  List,
  Item,
  Icon,
  BoxDate,
  TitleDate,
  ContainerDate,
  PickerContainerDay,
  PickerContainerMonth,
  PickerContainerYear,
  ButtonSend,
  ButtonSendText,
} from './styles';

import Footer from '~/components/Footer';

class Script extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  state = { selected: false };

  goPage = (page) => {
    const { navigation } = this.props;
    navigation.navigate(page);
  };

  select() {
    this.setState({ selected: !this.state.selected });
  }

  render() {
    const { navigation } = this.props;

    return (
      <Container source={background}>
        <ShortHeader source={shortHeader} />
        <ScrollView>
          <Center>
            <Grid>
              <BoxFilter source={backgroundRoteiro}>
                <Title>Atrações para divertir toda a família.</Title>
                <Description>
                  Aqui você pode conferir as atrações de acordo com as restrições de altura para se
                  divertir com segurança.
                </Description>
                <PickerContainer>
                  <Picker>
                    <Picker.Item label="Altura mínima: 1,40" value="Altura mínima: 1,40" />
                  </Picker>
                </PickerContainer>
              </BoxFilter>
              <List>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.select();
                  }}
                >
                  <Item source={itemBackground}>
                    <Icon source={this.state.selected ? selected : select} />
                  </Item>
                </TouchableWithoutFeedback>
              </List>
              <BoxDate source={backgroundRoteiro}>
                <TitleDate>Data prevista para sua visita:</TitleDate>
                <ContainerDate>
                  <PickerContainerDay>
                    <Picker>
                      <Picker.Item label="Seg, 17" value="17" />
                    </Picker>
                  </PickerContainerDay>
                  <PickerContainerMonth>
                    <Picker>
                      <Picker.Item label="Novembro" value="11" />
                    </Picker>
                  </PickerContainerMonth>
                  <PickerContainerYear>
                    <Picker>
                      <Picker.Item label="2019" value="2019" />
                    </Picker>
                  </PickerContainerYear>
                </ContainerDate>
                <ButtonSend
                  onPress={() => {
                    navigation.navigate('ScriptDetail');
                  }}
                >
                  <ButtonSendText>Finalizar Roteiro</ButtonSendText>
                </ButtonSend>
              </BoxDate>
            </Grid>
          </Center>
        </ScrollView>
        <Footer navigation={navigation} />
      </Container>
    );
  }
}

export default Script;
