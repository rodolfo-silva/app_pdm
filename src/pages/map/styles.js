import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
`;

export const Center = styled.View`
  flex-direction: row;
  justify-content: center;
`;

export const Content = styled.View`
  width: 90%;
  margin: 20px 0;
`;

export const Options = styled.View`
  background: #fff;
  flex-direction: row;
  justify-content: space-around;
  padding: 20px 0;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
`;

export const Item = styled.View`
  flex-direction: column;
  align-items: center;
`;

export const Icon = styled.Image``;

export const Label = styled.Text`
  margin-top: 5px;
  color: ${props => props.color};
  font-size: 12px;
  font-weight: 400;
  line-height: 15px;
  text-transform: uppercase;
`;

export const BoxMap = styled.ImageBackground`
  width: 100%;
  height: 445px;
`;

export const Info = styled.View`
  background-color: #ffcc02;
  padding: 20px;
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
`;

export const InfoContent = styled.View`
  background: #ffffff;
  border-radius: 5px;
  padding: 15px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const InfoContentTexts = styled.View`
  flex-direction: column;
`;

export const InfoTitle = styled.Text`
  color: #0572ba;
  font-size: 14px;
  font-weight: 400;
  line-height: 15px;
  margin-bottom: 10px;
`;

export const InfoDescription = styled.Text`
  color: #363636;
  font-size: 10px;
  font-weight: 400;
  margin-bottom: 10px;
`;

export const InfoObservation = styled.Text`
  color: #c02125;
  font-size: 9px;
  font-weight: 700;
`;

export const IconAttraction = styled.Image``;
