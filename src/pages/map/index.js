import React from 'react';
import PropTypes from 'prop-types';

import Header from '~/components/Header';
import Footer from '~/components/Footer';

import background from '~/images/map/background.png';
import iconAttractions from '~/images/map/iconAttractions.png';
import iconFood from '~/images/map/iconFood.png';
import iconServices from '~/images/map/iconServices.png';
import bgMap from '~/images/map/bgMap.png';
import horacic from '~/images/map/horacic.png';

import {
  Container,
  ScrollView,
  Center,
  Content,
  Options,
  Item,
  Icon,
  Label,
  BoxMap,
  Info,
  InfoContent,
  InfoContentTexts,
  InfoTitle,
  InfoDescription,
  InfoObservation,
  IconAttraction,
} from './styles';

const Map = ({ navigation }) => (
  <Container source={background}>
    <Header title="mapa e tempo da fila" navigation={navigation} historyBack="Main" />
    <ScrollView>
      <Center>
        <Content>
          <Options>
            <Item>
              <Icon source={iconAttractions} />
              <Label color="#c02125">atrações</Label>
            </Item>
            <Item>
              <Icon source={iconFood} />
              <Label color="#3fb049">alimentação</Label>
            </Item>
            <Item>
              <Icon source={iconServices} />
              <Label color="#0572ba">serviços</Label>
            </Item>
          </Options>
          <BoxMap source={bgMap} resizeMode="cover" />
          <Info>
            <InfoContent>
              <InfoContentTexts>
                <InfoTitle>Horacic Park</InfoTitle>
                <InfoDescription>Tempo mínimo de espera: 40 minutos</InfoDescription>
                <InfoObservation>Altura mínima: 1,40m</InfoObservation>
              </InfoContentTexts>
              <IconAttraction source={horacic} />
            </InfoContent>
          </Info>
        </Content>
      </Center>
    </ScrollView>
    <Footer navigation={navigation} />
  </Container>
);

Map.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default Map;
