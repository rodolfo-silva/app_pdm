/* eslint-disable react-native/no-raw-text */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import {
  Container, ScrollView, Content, ContentText,
} from './styles';

import Header from '~/components/Header';
import Footer from '~/components/Footer';

import background from '../../images/terms/background.png';

const Terms = ({ navigation }) => (
  <Fragment>
    <Header title="Poíticas e Termos" navigation={navigation} historyBack="Main" />
    <Container source={background} resizeMode="cover">
      <ScrollView>
        <Content>
          <ContentText>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vel lacus vel leo
            bibendum varius. Proin consectetur, nisi ultricies lacinia efficitur, sapien felis
            malesuada sem, ac tincidunt purus nisl eu orci. Mauris suscipit elit lacinia vestibulum
            imperdiet. Nulla eu nunc at nulla interdum fermentum. Praesent tristique pharetra ex at
            sodales. Ut efficitur ultrices risus, sed sagittis lacus sodales scelerisque. Vestibulum
            a iaculis sapien, non pulvinar nisl. Donec dignissim, odio vel tempus pellentesque,
            libero ante sagittis dolor, vitae tempus ante quam sed nisl. Sed sollicitudin venenatis
            ornare. Aliquam nisi neque, tincidunt pulvinar nibh ut, venenatis eleifend tortor.
            Maecenas pretium, mi eu faucibus eleifend, diam lectus aliquet neque, sed lacinia massa
            ipsum sed elit. Donec justo ligula, posuere in lectus vel, dapibus tristique lacus. Duis
            lorem diam, pharetra a justo vitae, cursus efficitur ligula. Donec mauris est, ultricies
            sodales elit sed, semper dapibus urna. Donec commodo ex at elit egestas faucibus.
            Maecenas sodales massa id turpis posuere venenatis. Orci varius natoque penatibus et
            magnis dis parturient montes, nascetur ridiculus mus. Aliquam suscipit pellentesque
            hendrerit. Nullam sagittis arcu quis odio sagittis vestibulum. Sed a aliquam nisl.
            Phasellus et malesuada nibh. Curabitur euismod orci vitae leo viverra commodo. Vivamus
            malesuada velit at enim convallis, vel ornare ante ornare. Nullam fringilla felis at
            tortor facilisis porttitor.
          </ContentText>
        </Content>
      </ScrollView>
    </Container>
    <Footer navigation={navigation} />
  </Fragment>
);

Terms.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default Terms;
