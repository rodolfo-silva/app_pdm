import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
  padding: 20px;
`;

export const Content = styled.View`
  background: #fff;
  padding: 20px;
  border-radius: 5px;
  margin-bottom: 30px;
`;

export const ContentText = styled.Text`
  color: #363636;
  font-size: 11px;
  font-weight: bold;
  line-height: 16px;
`;
