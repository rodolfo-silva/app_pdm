/* eslint-disable react-native/no-raw-text */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { TouchableWithoutFeedback } from 'react-native';

// Images
import background from '~/images/background.png';
import buttonClose from '~/images/icons/buttonClose.png';

import {
  Container,
  ScrollView,
  CenterContent,
  Form,
  HeaderForm,
  ImageClose,
  TextHeaderForm,
  ContentForm,
  InfoLabel,
  Label,
  TextInput,
  ButtonSend,
  ButtonSendText,
} from './styles';

class ForgotPassword extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  goPage = (page) => {
    const { navigation } = this.props;
    navigation.navigate(page);
  };

  render() {
    return (
      <Container source={background}>
        <ScrollView>
          <CenterContent>
            <Form>
              <HeaderForm>
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.goPage('Login');
                  }}
                >
                  <ImageClose source={buttonClose} />
                </TouchableWithoutFeedback>
                <TextHeaderForm>esqueceu sua senha?</TextHeaderForm>
              </HeaderForm>
              <ContentForm>
                <InfoLabel>
                  Digite seu email que vamos enviar uma nova redefinição de senha
                </InfoLabel>
                <Label>Email</Label>
                <TextInput
                  autoCapitalize="none"
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                />
                <ButtonSend>
                  <ButtonSendText>enviar</ButtonSendText>
                </ButtonSend>
              </ContentForm>
            </Form>
          </CenterContent>
        </ScrollView>
      </Container>
    );
  }
}

export default ForgotPassword;
