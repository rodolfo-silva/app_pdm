import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
`;

export const ShortHeader = styled.ImageBackground`
  width: 100%;
  height: 99px;
  margin-bottom: 20px;
`;

export const ButtonBack = styled.TouchableOpacity`
  width: 19px;
  height: 15px;
  margin-left: 25px;
  margin-top: 35px;
`;

export const CenterContent = styled.View`
  flex: 1;
  align-items: center;
`;

export const Form = styled.View`
  width: 90%;
  justify-content: center;
  border-radius: 6px;
  background-color: #fffde8;
  margin-top: 70px;
`;

export const HeaderForm = styled.View`
  justify-content: center;
  width: 100%;
  height: 55px;
  margin-bottom: 30px;
`;

export const ImageClose = styled.Image`
  margin-left: 15px;
  margin-top: 15px;
`;

export const TextHeaderForm = styled.Text`
  font-size: 18px;
  color: #ed2626;
  text-align: center;
  font-weight: 700;
  text-transform: uppercase;
`;

export const ContentForm = styled.View`
  justify-content: center;
  align-items: center;
  width: 100%;
  padding: 20px;
  flex-direction: column;
`;

export const InfoLabel = styled.Text`
  color: #363636;
  font-size: 14px;
  font-weight: 700;
  margin-bottom: 20px;
  text-align: center;
`;

export const Label = styled.Text`
  color: #363636;
  font-size: 14px;
  font-weight: 700;
  margin-bottom: 5px;
  text-align: left;
  width: 100%;
`;

export const TextInput = styled.TextInput.attrs({
  placeholderTextColor: '#a4a4a4',
  fontWeight: 'bold',
})`
  width: 100%;
  height: 48px;
  border-radius: 6px;
  border: 1px solid #2173a1;
  background-color: #ffffff;
  margin-bottom: 20px;
  padding: 0 20px;
`;

export const ButtonSend = styled.TouchableOpacity`
  border-radius: 6px;
  background-color: #ed2626;
  width: 200px;
  height: 48px;
  align-items: center;
  justify-content: center;
`;

export const ButtonSendText = styled.Text`
  color: #ffffff;
  font-size: 16px;
  font-weight: 700;
  text-transform: uppercase;
`;
