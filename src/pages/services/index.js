/* eslint-disable react-native/no-raw-text */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import {
  Container,
  ScrollView,
  Content,
  Intro,
  IntroText,
  Options,
  Label,
  List,
  ListItem,
  Square,
  ListItemText,
  Division,
  DivisionText,
  ContainerBaby,
  TextRow,
} from './styles';

import Header from '~/components/Header';
import Footer from '~/components/Footer';

import background from '../../images/terms/background.png';

const Services = ({ navigation }) => (
  <Fragment>
    <Header title="Serviços" navigation={navigation} historyBack="Main" />
    <Container source={background} resizeMode="cover">
      <ScrollView>
        <Content>
          <Options>
            <Intro>
              <IntroText>
                Com superatrações dentro de uma área de 12 mil m², o Parque da Mônica possui total
                infraestrutura, conforto e segurança para que todos os visitantes tenham momentos
                inesquecíveis. Tudo isso e muito mais, ao lado da Mônica, Cebolinha, Magali e
                Cascão. Já pensou que incrível?
              </IntroText>
            </Intro>
            <List>
              <Label>Dispomos de:</Label>
              <ListItem>
                <Square />
                <ListItemText>Carrinhos de bebê*</ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>Sanitários</ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>Rede wi-fi gratuita</ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>Achados e Perdidos</ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>Acesso para deficientes</ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>Enfermaria</ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>
                  Espaço Família: trocadores, sala de amamentação, sala de papinha e banheiro
                  família
                </ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>SAV - Serviço de Atendimento ao Visitante</ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>Guarda Volumes*</ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>Estacionamento no Shopping SP Market*</ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>Praça de Alimentação*</ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>
                  Três salões de festas exclusivos para Festas de Aniversário
                </ListItemText>
              </ListItem>
              <ListItem>
                <Square />
                <ListItemText>Loja do Parque da Mônica*</ListItemText>
              </ListItem>
              <ListItem>
                <ListItemText light>*Pagos à parte</ListItemText>
              </ListItem>
            </List>
            <Division>
              <DivisionText>Carrinhos de bebê</DivisionText>
            </Division>
            <ContainerBaby>
              <TextRow head>Locação Carrinhos de Bebê - R$ 15,00*</TextRow>

              <TextRow>
                *R$ 5,00 caução em dinheiro + R$ 10,00 locação. Na devolução do carrinho é realizada
                a devolução da caução ao visitante.
              </TextRow>
              <TextRow head>Estacionamento para Carrinho de Bebê - R$ 10,00</TextRow>
              <TextRow>Informações Gerais:</TextRow>
              <TextRow>
                Após utilizar o carrinho de bebê alugado, por favor devolva ao local de retirada.
              </TextRow>
            </ContainerBaby>
          </Options>
        </Content>
      </ScrollView>
    </Container>
    <Footer navigation={navigation} />
  </Fragment>
);

Services.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default Services;
