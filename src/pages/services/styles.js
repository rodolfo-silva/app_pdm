import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  flex: 1;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
  padding: 20px;
`;

export const Content = styled.View`
  background: #fff;
  border-radius: 5px;
  margin-bottom: 30px;
  background: #fff;
`;

export const Intro = styled.View`
  background: #3fb049;
  padding: 20px;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
`;

export const IntroText = styled.Text`
  color: #ffffff;
  font-size: 11px;
  font-weight: 700;
  line-height: 15px;
`;

export const Options = styled.View``;

export const List = styled.View`
  padding: 20px;
`;

export const Label = styled.Text`
  color: #363636;
  font-size: 11px;
  font-weight: 700;
  line-height: 15px;
  margin-bottom: 15px;
`;

export const ListItem = styled.View`
  margin-bottom: 10px;
  flex-direction: row;
  align-items: center;
`;

export const Square = styled.View`
  width: 6px;
  height: 6px;
  background: #3fb049;
  border-radius: 3px;
`;

export const ListItemText = styled.Text`
  color: ${props => (props.light ? '#8a8a8a' : '#363636')};
  font-size: ${props => (props.light ? '9px' : '11px')};
  font-weight: 700;
  line-height: 19px;
  margin-left: ${props => (props.light ? '20px' : '15px')};
`;

export const Division = styled.View`
  padding: 10px 0;
  background: #3fb049;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const DivisionText = styled.Text`
  color: #fbe926;
  font-size: 11px;
  font-weight: 700;
`;

export const ContainerBaby = styled.View`
  padding: 20px;
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
`;

export const TextRow = styled.Text`
  color: #363636;
  font-size: ${props => (props.head ? '11px' : '9px')};
  line-height: ${props => (props.head ? '14px' : '12px')};
  font-weight: 700;
  margin-bottom: 15px;
`;
