import React, { Component } from 'react';
import CodePush from 'react-native-code-push';

import '~/config/ReactotronConfig';
import '~/config/DevToolsConfig';

import Routes from '~/routes';

import Syncronizer from '~/pages/syncronizer';

class App extends Component {
  state = {
    completed: false,
    message: '',
  };

  componentDidMount() {
    CodePush.sync(
      { installMode: CodePush.InstallMode.IMMEDIATE, updateDialog: false },
      this.handleCodePushStatusChange,
      this.handleCodePushDownloadComplete,
    );
  }

  handleCodePushStatusChange = (status) => {
    switch (status) {
      case CodePush.SyncStatus.CHECKING_FOR_UPDATE:
        this.setState({ message: 'Verificando Atualizações ...' });
        break;
      case CodePush.SyncStatus.DOWNLOADING_PACKAGE:
        this.setState({ message: 'Baixando Atualizações ...' });
        break;
      case CodePush.SyncStatus.INSTALLING_UPDATE:
        this.setState({ message: 'Instalando Atualizações ...' });
        break;
      default:
        this.setState({ completed: true });
    }
  };

  handleCodePushDownloadComplete = () => {
    this.setState({ completed: false });
  };

  render() {
    const { completed, message } = this.state;
    return completed ? <Routes /> : <Syncronizer message={message} />;
  }
}

export default CodePush({
  checkFrequency: CodePush.CheckFrequency.MANUAL,
})(App);
