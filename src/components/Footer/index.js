import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import backgroundFooter from '~/images/footer/backgroundFooter.png';
import iconHome from '~/images/footer/iconHome.png';
import iconMap from '~/images/footer/iconMap.png';
import iconFastMenu from '~/images/footer/iconFastMenu.png';
import iconCloseFastMenu from '~/images/footer/iconCloseFastMenu.png';
import iconPin from '~/images/footer/iconPin.png';
import iconMenu from '~/images/footer/iconMenu.png';

import { TouchableWithoutFeedback, Dimensions } from 'react-native';

import { Container, BoxIcons, Icon } from './styles';

import FastMenu from './FastMenu';
import CoreMenu from './CoreMenu';

const { height } = Dimensions.get('window');
const heightMenu = height - 190;

class Footer extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
  };

  state = {
    stateFastMenu: false,
    stateCoreMenu: false,
    fastMenuIcon: iconFastMenu,
  };

  goPage = (page) => {
    const { navigation } = this.props;
    navigation.navigate(page);
  };

  toggleFastMenu = () => {
    const { stateFastMenu } = this.state;
    const state = !stateFastMenu;
    const icon = stateFastMenu ? iconFastMenu : iconCloseFastMenu;
    this.setState({ fastMenuIcon: icon, stateFastMenu: state, stateCoreMenu: false });
  };

  toggleCoreMenu = () => {
    const { stateCoreMenu } = this.state;
    const state = !stateCoreMenu;
    this.setState({ stateCoreMenu: state, stateFastMenu: false, fastMenuIcon: iconFastMenu });
  };

  render() {
    const { fastMenuIcon, stateFastMenu, stateCoreMenu } = this.state;
    const { navigation } = this.props;

    return (
      <Fragment>
        <Container source={backgroundFooter} resizeMode="stretch">
          {stateFastMenu && <FastMenu navigation={navigation} height={heightMenu} />}
          {stateCoreMenu && <CoreMenu navigation={navigation} height={heightMenu} />}
          <BoxIcons>
            <TouchableWithoutFeedback
              onPress={() => {
                this.goPage('Home');
              }}
            >
              <Icon source={iconHome} />
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback
              onPress={() => {
                this.goPage('Calendar');
              }}
            >
              <Icon source={iconMap} />
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={this.toggleFastMenu}>
              <Icon source={fastMenuIcon} />
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback
              onPress={() => {
                this.goPage('Map');
              }}
            >
              <Icon source={iconPin} />
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={this.toggleCoreMenu}>
              <Icon source={iconMenu} />
            </TouchableWithoutFeedback>
          </BoxIcons>
        </Container>
      </Fragment>
    );
  }
}

export default Footer;
