import React from 'react';
import PropTypes from 'prop-types';

import { TouchableWithoutFeedback } from 'react-native';

import {
  Container,
  ScrollView,
  HelloText,
  List,
  ItemList,
  ItemListText,
  Icon,
  MyAccount,
  MyAccountHeader,
  MyAccountHeaderTitle,
  ContainerEdit,
  IconEdit,
  TextEdit,
  MyAccountContent,
  MyAccountRow,
  MyAccountIcon,
  ColumnContent,
  Number,
  Description,
} from './styles';

import background from '../../../images/footer/coreMenu/background.png';
import arrowIcon from '../../../images/footer/coreMenu/arrowIcon.png';
import iconEdit from '../../../images/footer/coreMenu/iconEdit.png';
import iconPhone from '../../../images/contact/iconPhone.png';
import iconEmail from '../../../images/contact/iconEmail.png';

const CoreMenu = ({ height, navigation }) => (
  <Container height={height} source={background}>
    <ScrollView>
      <HelloText>Olá, Pedro da Silva</HelloText>
      <List>
        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Location');
          }}
        >
          <ItemList bg="#ffcc02" first>
            <ItemListText>localização</ItemListText>
            <Icon source={arrowIcon} />
          </ItemList>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Services');
          }}
        >
          <ItemList bg="#3fb049">
            <ItemListText>serviços</ItemListText>
            <Icon source={arrowIcon} />
          </ItemList>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Faq');
          }}
        >
          <ItemList bg="#43c8f3">
            <ItemListText>faq - perguntas e respostas</ItemListText>
            <Icon source={arrowIcon} />
          </ItemList>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Contact');
          }}
        >
          <ItemList bg="#ffe602">
            <ItemListText>contato</ItemListText>
            <Icon source={arrowIcon} />
          </ItemList>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Terms');
          }}
        >
          <ItemList bg="#ff5656" last>
            <ItemListText>políticas e termos de uso</ItemListText>
            <Icon source={arrowIcon} />
          </ItemList>
        </TouchableWithoutFeedback>
      </List>
      <MyAccount>
        <MyAccountHeader>
          <MyAccountHeaderTitle>minha conta</MyAccountHeaderTitle>
          <ContainerEdit>
            <IconEdit source={iconEdit} />
            <TextEdit>Editar perfil</TextEdit>
          </ContainerEdit>
        </MyAccountHeader>
        <MyAccountContent>
          <MyAccountRow haveBorder>
            <MyAccountIcon source={iconPhone} />
            <ColumnContent>
              <Number>+123 456 789 234</Number>
              <Description>Mobile</Description>
            </ColumnContent>
          </MyAccountRow>
          <MyAccountRow>
            <MyAccountIcon source={iconEmail} />
            <ColumnContent>
              <Number>john.doe@mail.com</Number>
              <Description>email</Description>
            </ColumnContent>
          </MyAccountRow>
        </MyAccountContent>
      </MyAccount>
    </ScrollView>
  </Container>
);

CoreMenu.propTypes = {
  height: PropTypes.number.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default CoreMenu;
