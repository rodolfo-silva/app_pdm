import styled from 'styled-components';

export const Container = styled.ImageBackground`
  width: 100%;
  height: ${props => props.height || '450px'};
  position: absolute;
  bottom: 90px;
  flex-direction: column;
`;

export const ScrollView = styled.ScrollView`
  flex: 1;
  padding: 40px 20px;
`;

export const HelloText = styled.Text`
  color: #363636;
  font-size: 18px;
  font-weight: 700;
  margin-left: 20px;
`;

export const List = styled.View`
  margin: 20px 0 15px;
  border-radius: 4px;
`;

export const ItemList = styled.View`
  padding: 20px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background: ${props => props.bg || '#ccc'};
  border-top-left-radius: ${props => (props.first ? '5px' : '0')};
  border-top-right-radius: ${props => (props.first ? '5px' : '0')};
  border-bottom-left-radius: ${props => (props.last ? '5px' : '0')};
  border-bottom-right-radius: ${props => (props.last ? '5px' : '0')};
`;

export const ItemListText = styled.Text`
  color: #363636;
  font-size: 16px;
  font-weight: 400;
`;

export const Icon = styled.Image``;

export const MyAccount = styled.View`
  margin: 10px 0 60px;
`;

export const MyAccountHeader = styled.View`
  background: #c02125;
  padding: 10px 20px;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  flex-direction: row;
  justify-content: space-between;
`;

export const MyAccountHeaderTitle = styled.Text`
  color: #ffffff;
  font-size: 16px;
  font-weight: 700;
`;

export const ContainerEdit = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const IconEdit = styled.Image``;

export const TextEdit = styled.Text`
  color: #ffffff;
  font-size: 10px;
  font-weight: 700;
  text-decoration: underline;
  margin-left: 5px;
`;

export const MyAccountContent = styled.View`
  background: #fff;
  border-bottom-left-radius: 5px;
  border-bottom-right-radius: 5px;
`;

export const MyAccountRow = styled.View`
  padding: 10px 20px;
  flex-direction: row;
  align-items: center;
  border-bottom-width: ${props => (props.haveBorder ? '1px' : '0')};
  border-bottom-color: ${props => (props.haveBorder ? '#e1e1e1' : '#fff')};
`;

export const MyAccountIcon = styled.Image``;

export const ColumnContent = styled.View`
  margin-left: 20px;
`;

export const Number = styled.Text`
  color: #363636;
  font-size: 14px;
  font-weight: 700;
`;

export const Description = styled.Text`
  color: #363636;
  font-size: 11px;
  font-weight: 700;
`;
