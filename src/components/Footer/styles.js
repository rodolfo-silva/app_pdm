import styled from 'styled-components/native';

export const Container = styled.ImageBackground`
  width: 100%;
  height: 90px;
  position: relative;
`;

export const BoxIcons = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  padding-top: 10px;
`;

export const Icon = styled.Image`
  margin: 0 5px;
`;
