import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { TouchableWithoutFeedback } from 'react-native';
import bgAttractions from '../../../images/footer/fastMenu/bgAttractions.png';
import bgQRcode from '../../../images/footer/fastMenu/bgQRcode.png';
import bgTreasure from '../../../images/footer/fastMenu/bgTreasure.png';
import bgMyCollections from '../../../images/footer/fastMenu/bgMyCollections.png';
import bgVideoteca from '../../../images/footer/fastMenu/bgVideoteca.png';

import {
  Container, Background, List, Image,
} from './styles';

const FastMenu = ({ height, navigation }) => (
  <Fragment>
    <Background height={height} />
    <Container height={height}>
      <List>
        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Attractions');
          }}
        >
          <Image source={bgAttractions} resizeMode="contain" />
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Scancode');
          }}
        >
          <Image source={bgQRcode} />
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Treasure');
          }}
        >
          <Image source={bgTreasure} />
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Achievements');
          }}
        >
          <Image source={bgMyCollections} />
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Videolibrary');
          }}
        >
          <Image source={bgVideoteca} />
        </TouchableWithoutFeedback>
      </List>
    </Container>
  </Fragment>
);

FastMenu.propTypes = {
  height: PropTypes.number.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default FastMenu;
