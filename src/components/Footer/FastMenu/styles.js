import styled from 'styled-components/native';

import LinearGradient from 'react-native-linear-gradient';

export const Background = styled.View`
  width: 100%;
  height: ${props => props.height || '450px'};
  position: absolute;
  background: #f2f2f2;
  opacity: 0.8;
  bottom: 90px;
`;

export const Container = styled.View`
  width: 100%;
  height: ${props => props.height || '450px'};
  position: absolute;
  bottom: 90px;
`;

export const List = styled.View`
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  padding: 40px 30px 10px;
`;

export const ItemList = styled(LinearGradient)`
  width: 100%;
  height: 70px;
  flex-direction: row;
  align-items: center;
  border-radius: 6px;
  margin-bottom: 10px;
  padding-left: 20px;
`;

export const TextItemList = styled.Text`
  color: #ffed00;
  font-size: 18px;
  font-weight: 400;
  justify-content: center;
  align-items: center;
  margin-left: 20px;
`;

export const Image = styled.ImageBackground`
  width: 100%;
  height: 20%;
`;
