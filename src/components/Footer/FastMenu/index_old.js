/* eslint-disable react-native/no-raw-text */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import { Image, TouchableWithoutFeedback } from 'react-native';
import attractions from '../../../images/footer/fastMenu/attractions.png';
import qrCode from '../../../images/footer/fastMenu/qrCode.png';
import treasure from '../../../images/footer/fastMenu/treasure.png';
import myCollections from '../../../images/footer/fastMenu/myCollections.png';
import videoteca from '../../../images/footer/fastMenu/videoteca.png';

import {
  Container, Background, List, ItemList, TextItemList,
} from './styles';

const colorsItemList = ['#00c1ff', '#0b78bf'];

const FastMenu = ({ height, navigation }) => (
  <Fragment>
    <Background height={height} />
    <Container height={height}>
      <List>
        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Attractions');
          }}
        >
          <ItemList colors={colorsItemList}>
            <Image source={attractions} />
            <TextItemList>Atrações</TextItemList>
          </ItemList>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Scancode');
          }}
        >
          <ItemList colors={colorsItemList}>
            <Image source={qrCode} />
            <TextItemList>Scanear QR Code</TextItemList>
          </ItemList>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Treasure');
          }}
        >
          <ItemList colors={colorsItemList}>
            <Image source={treasure} />
            <TextItemList>Caça ao Tesouro</TextItemList>
          </ItemList>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Achievements');
          }}
        >
          <ItemList colors={colorsItemList}>
            <Image source={myCollections} />
            <TextItemList>Minhas Coleções</TextItemList>
          </ItemList>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate('Videolibrary');
          }}
        >
          <ItemList colors={colorsItemList}>
            <Image source={videoteca} />
            <TextItemList>Videoteca</TextItemList>
          </ItemList>
        </TouchableWithoutFeedback>
      </List>
    </Container>
  </Fragment>
);

FastMenu.propTypes = {
  height: PropTypes.number.isRequired,
  navigation: PropTypes.shape({
    navigate: PropTypes.func,
  }).isRequired,
};

export default FastMenu;
