import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  height: 99px;
  z-index: 1;
`;

export const Background = styled.ImageBackground`
  width: 100%;
  height: 99px;
  padding-top: 20px;
  align-items: center;
`;

export const Title = styled.Text`
  color: #ed2626;
  font-size: 24px;
  font-weight: 400;
  text-transform: uppercase;
`;

export const ButtonBack = styled.TouchableOpacity`
  width: 19px;
  height: 15px;
  left: 20px;
  top: 30px;
  position: absolute;
`;
