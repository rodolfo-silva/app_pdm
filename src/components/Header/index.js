import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Image } from 'react-native';
import {
  Container, Background, Title, ButtonBack,
} from './styles';

import backgroundClearHeader from '~/images/backgroundClearHeader.png';
import buttonBack from '~/images/icons/buttonBack.png';

class Header extends Component {
  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func,
    }).isRequired,
    title: PropTypes.string.isRequired,
    historyBack: PropTypes.string.isRequired,
  };

  goPage = (page) => {
    const { navigation } = this.props;
    navigation.navigate(page);
  };

  render() {
    const { title, historyBack } = this.props;

    return (
      <Container>
        <Background source={backgroundClearHeader}>
          <ButtonBack
            onPress={() => {
              this.goPage(historyBack);
            }}
          >
            <Image source={buttonBack} />
          </ButtonBack>
          <Title>{title}</Title>
        </Background>
      </Container>
    );
  }
}

export default Header;
