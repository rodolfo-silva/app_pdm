import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import Main from '~/pages/main';
import CreateAccount from '~/pages/createAccount';
import Login from '~/pages/login';
import ForgotPassword from '~/pages/forgotPassword';
import Introduction from '~/pages/introduction';
import Home from '~/pages/home';
import Programming from '~/pages/programming';
import Calendar from '~/pages/calendar';
import Attractions from '~/pages/attractions';
import AttractionsDetail from '~/pages/attractions/detail';
import Map from '~/pages/map';
import Script from '~/pages/script';
import Treasure from '~/pages/treasure';
import Achievements from '~/pages/achievements';
import Profile from '~/pages/profile';
import Location from '~/pages/location';
import Services from '~/pages/services';
import Faq from '~/pages/faq';
import Contact from '~/pages/contact';
import Terms from '~/pages/terms';
import Videolibrary from '~/pages/videolibrary';
import VideolibraryDetail from '~/pages/videolibrary/detail';
import Scancode from '~/pages/scancode';

const Routes = createAppContainer(
  createSwitchNavigator({
    Main,
    CreateAccount,
    ForgotPassword,
    Login,
    Introduction,
    Home,
    Programming,
    Calendar,
    Attractions,
    AttractionsDetail,
    Map,
    Script,
    Treasure,
    Achievements,
    Profile,
    Location,
    Services,
    Faq,
    Contact,
    Terms,
    Videolibrary,
    VideolibraryDetail,
    Scancode,
  }),
);

export default Routes;
