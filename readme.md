# Aplicativo do Parque da Mônica - Front End

## Tecnologias utilizadas

- React Native 0.59
- Styled Components
- Reactotron (Debug)

## Dependências

- Node JS
- Yarn
- React Native CLI
- JDK (Java Development Kit)
- Libs Gráficas 32 bits (Linux)

## Emulador

Ele tem o **Virtual Box** como dependência.

- [Genymotion](https://www.genymotion.com/fun-zone/)

## Rodando o projeto (com o emulador aberto)

Na primeira vez que você for rodar o projeto:

- Instalando as dependências `yarn`
- Buildar o projeto `react-native run-android` ou `react-native run-ios`

Uma vez feito o build do projeto no emulador:

- Rodar o projeto `react-native start`
